#include <stdio.h>
#include <string.h>

#include "driver_logic.c"
#include "unity.h"

void setUp(void) {
  internal_sensor_data.SENSOR_SONARS_left = 100;
  internal_sensor_data.SENSOR_SONARS_middle = 100;
  internal_sensor_data.SENSOR_SONARS_right = 100;
  internal_sensor_data.SENSOR_SONARS_rear = 100;
  internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig = 10;
  internal_motor_data.SERVO_STEER_ANGLE_sig = 0;
}

void test_driver__process_input(void) {
  dbc_SENSOR_SONARS_ROUTINE_s data_to_send_to_func = {
      .SENSOR_SONARS_left = 30, .SENSOR_SONARS_middle = 19, .SENSOR_SONARS_right = 20, .SENSOR_SONARS_rear = 51};
  driver__process_input(&data_to_send_to_func);
  TEST_ASSERT_EQUAL(data_to_send_to_func.SENSOR_SONARS_left, internal_sensor_data.SENSOR_SONARS_left);
  TEST_ASSERT_EQUAL(data_to_send_to_func.SENSOR_SONARS_right, internal_sensor_data.SENSOR_SONARS_right);
  TEST_ASSERT_EQUAL(data_to_send_to_func.SENSOR_SONARS_middle, internal_sensor_data.SENSOR_SONARS_middle);
  TEST_ASSERT_EQUAL(data_to_send_to_func.SENSOR_SONARS_rear, internal_sensor_data.SENSOR_SONARS_rear);
}

void test_left_object_detected(void) {
  internal_sensor_data.SENSOR_SONARS_left = 0;
  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(internal_motor_data.SERVO_STEER_ANGLE_sig, 5);
  TEST_ASSERT_EQUAL(internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig, 5);
}
void test_left_object_detected_check_max_threshold(void) {
  internal_sensor_data.SENSOR_SONARS_left = 7;
  internal_motor_data.SERVO_STEER_ANGLE_sig = 86;
  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(internal_motor_data.SERVO_STEER_ANGLE_sig, 45);
  TEST_ASSERT_EQUAL(internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig, 5);
}
void test_left_object_detected_check_max_threshold_part_2(void) {
  internal_sensor_data.SENSOR_SONARS_left = 5;
  internal_motor_data.SERVO_STEER_ANGLE_sig = 85;
  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(internal_motor_data.SERVO_STEER_ANGLE_sig, 45);
  TEST_ASSERT_EQUAL(internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig, 5);
}

void test_right_object_detected(void) {
  internal_sensor_data.SENSOR_SONARS_right = 7;
  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(internal_motor_data.SERVO_STEER_ANGLE_sig, -5);
  TEST_ASSERT_EQUAL(internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig, 5);
}
void test_right_object_detected_check_max_threshold(void) {
  internal_sensor_data.SENSOR_SONARS_right = 9;
  internal_motor_data.SERVO_STEER_ANGLE_sig = -86;
  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(internal_motor_data.SERVO_STEER_ANGLE_sig, -45);
  TEST_ASSERT_EQUAL(internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig, 5);
}
void test_right_object_detected_check_max_threshold_part_2(void) {
  internal_sensor_data.SENSOR_SONARS_right = 12;
  internal_motor_data.SERVO_STEER_ANGLE_sig = -85;
  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(internal_motor_data.SERVO_STEER_ANGLE_sig, -45);
  TEST_ASSERT_EQUAL(internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig, 5);
}

void test_middle_object_only_detected_equal_left_and_right_sensor(void) {
  internal_sensor_data.SENSOR_SONARS_middle = 2;
  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(internal_motor_data.SERVO_STEER_ANGLE_sig, 10);
  TEST_ASSERT_EQUAL(internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig, -5);
}

void test_middle_object_only_detected_left_object_closer(void) {
  internal_sensor_data.SENSOR_SONARS_middle = 7;
  internal_sensor_data.SENSOR_SONARS_left = 12;
  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(internal_motor_data.SERVO_STEER_ANGLE_sig, 5);
  TEST_ASSERT_EQUAL(internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig, 5);
}

void test_middle_object_only_detected_right_object_closer(void) {
  internal_sensor_data.SENSOR_SONARS_middle = 5;
  internal_sensor_data.SENSOR_SONARS_right = 12;
  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(internal_motor_data.SERVO_STEER_ANGLE_sig, -5);
  TEST_ASSERT_EQUAL(internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig, 5);
}

void test_rear_object_detected_left_and_right_equal(void) {
  internal_sensor_data.SENSOR_SONARS_rear = 5;
  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(internal_motor_data.SERVO_STEER_ANGLE_sig, 5);
  TEST_ASSERT_EQUAL(internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig, 10);
}
void test_rear_object_detected_left_object_detected(void) {
  internal_sensor_data.SENSOR_SONARS_rear = 5;
  internal_sensor_data.SENSOR_SONARS_left = 12;
  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(internal_motor_data.SERVO_STEER_ANGLE_sig, 5);
  TEST_ASSERT_EQUAL(internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig, 5);
}
void test_rear_object_detected_right_object_detected(void) {
  internal_sensor_data.SENSOR_SONARS_rear = 5;
  internal_sensor_data.SENSOR_SONARS_right = 12;
  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(internal_motor_data.SERVO_STEER_ANGLE_sig, -5);
}

void test_all_front_sensors_detected(void) {
  internal_sensor_data.SENSOR_SONARS_rear = 5;
  internal_sensor_data.SENSOR_SONARS_right = 12;
  internal_sensor_data.SENSOR_SONARS_left = 3;
  internal_sensor_data.SENSOR_SONARS_middle = 1;
  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(internal_motor_data.SERVO_STEER_ANGLE_sig, 10);
  TEST_ASSERT_EQUAL(internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig, -5);
}

void test_no_sensors_triggered_go_max_speed(void) {
  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(internal_motor_data.SERVO_STEER_ANGLE_sig, 0);
  TEST_ASSERT_EQUAL(internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig, 5);
}

void test_turn_towards_destination_same_Quad_one(void) {
  received_heading.CURRENT_HEADING = 25;
  received_heading.DESTINATION_HEADING = 75;
  turn_towards_destination();
  TEST_ASSERT_EQUAL(heading_difference, 50);
  TEST_ASSERT_EQUAL(turn_direction, right);
}
void test_turn_towards_destination_same_Quad_two(void) {
  received_heading.CURRENT_HEADING = 100;
  received_heading.DESTINATION_HEADING = 125;
  turn_towards_destination();
  TEST_ASSERT_EQUAL(heading_difference, 25);
  TEST_ASSERT_EQUAL(turn_direction, right);
}
void test_turn_towards_destination_same_Quad_three(void) {
  received_heading.CURRENT_HEADING = 195;
  received_heading.DESTINATION_HEADING = 255;
  turn_towards_destination();
  TEST_ASSERT_EQUAL(heading_difference, 60);
  TEST_ASSERT_EQUAL(turn_direction, right);
}
void test_turn_towards_destination_same_Quad_four(void) {
  received_heading.CURRENT_HEADING = 295;
  received_heading.DESTINATION_HEADING = 300;
  turn_towards_destination();
  TEST_ASSERT_EQUAL(heading_difference, 5);
  TEST_ASSERT_EQUAL(turn_direction, right);
}
void test_turn_towards_destination_Quad_one_to_two(void) {
  received_heading.CURRENT_HEADING = 25;
  received_heading.DESTINATION_HEADING = 150;
  turn_towards_destination();
  TEST_ASSERT_EQUAL(heading_difference, 125);
  TEST_ASSERT_EQUAL(turn_direction, right);
}
void test_turn_towards_destination_Quad_one_to_three(void) {
  received_heading.CURRENT_HEADING = 25;
  received_heading.DESTINATION_HEADING = 200;
  turn_towards_destination();
  TEST_ASSERT_EQUAL(heading_difference, 175);
  TEST_ASSERT_EQUAL(turn_direction, right);
}
void test_turn_towards_destination_Quad_one_to_three_reflection(void) {
  received_heading.CURRENT_HEADING = 25;
  received_heading.DESTINATION_HEADING = 240;
  turn_towards_destination();
  TEST_ASSERT_EQUAL(heading_difference, 145);
  TEST_ASSERT_EQUAL(turn_direction, left);
}
void test_turn_towards_destination_Quad_one_to_four(void) {
  received_heading.CURRENT_HEADING = 25;
  received_heading.DESTINATION_HEADING = 300;
  turn_towards_destination();
  TEST_ASSERT_EQUAL(heading_difference, 85);
  TEST_ASSERT_EQUAL(turn_direction, left);
}
void test_turn_towards_destination_Quad_two_to_one(void) {
  received_heading.CURRENT_HEADING = 110;
  received_heading.DESTINATION_HEADING = 65;
  turn_towards_destination();
  TEST_ASSERT_EQUAL(heading_difference, 45);
  TEST_ASSERT_EQUAL(turn_direction, left);
}
void test_turn_towards_destination_Quad_two_to_three(void) {
  received_heading.CURRENT_HEADING = 95;
  received_heading.DESTINATION_HEADING = 265;
  turn_towards_destination();
  TEST_ASSERT_EQUAL(heading_difference, 170);
  TEST_ASSERT_EQUAL(turn_direction, right);
}
void test_turn_towards_destination_Quad_two_to_four(void) {
  received_heading.CURRENT_HEADING = 95;
  received_heading.DESTINATION_HEADING = 295;
  turn_towards_destination();
  TEST_ASSERT_EQUAL(heading_difference, 160);
  TEST_ASSERT_EQUAL(turn_direction, left);
}
void test_turn_towards_destination_Quad_three_to_one(void) {
  received_heading.CURRENT_HEADING = 190;
  received_heading.DESTINATION_HEADING = 5;
  turn_towards_destination();
  TEST_ASSERT_EQUAL(heading_difference, 175);
  TEST_ASSERT_EQUAL(turn_direction, right);
}
void test_turn_towards_destination_Quad_three_to_two(void) {
  received_heading.CURRENT_HEADING = 200;
  received_heading.DESTINATION_HEADING = 120;
  turn_towards_destination();
  TEST_ASSERT_EQUAL(heading_difference, 80);
  TEST_ASSERT_EQUAL(turn_direction, left);
}
void test_turn_towards_destination_Quad_three_to_four(void) {
  received_heading.CURRENT_HEADING = 200;
  received_heading.DESTINATION_HEADING = 300;
  turn_towards_destination();
  TEST_ASSERT_EQUAL(heading_difference, 100);
  TEST_ASSERT_EQUAL(turn_direction, right);
}
void test_turn_towards_destination_Quad_four_to_one(void) {
  received_heading.CURRENT_HEADING = 300;
  received_heading.DESTINATION_HEADING = 40;
  turn_towards_destination();
  TEST_ASSERT_EQUAL(heading_difference, 100); // The wrap around should make this a postive angle turn
  TEST_ASSERT_EQUAL(turn_direction, right);
}
void test_turn_towards_destination_Quad_four_to_two_left_is_best(void) {
  received_heading.CURRENT_HEADING = 295;
  received_heading.DESTINATION_HEADING = 135;
  turn_towards_destination();
  TEST_ASSERT_EQUAL(heading_difference, 160);
  TEST_ASSERT_EQUAL(turn_direction, left);
}
void test_turn_towards_destination_Quad_four_to_two_right_is_best(void) {
  received_heading.CURRENT_HEADING = 325;
  received_heading.DESTINATION_HEADING = 135;
  turn_towards_destination();
  TEST_ASSERT_EQUAL(heading_difference, 170);
  TEST_ASSERT_EQUAL(turn_direction, right); // 0 is right left is 1
}
void test_turn_towards_destination_Quad_four_to_three(void) {
  received_heading.CURRENT_HEADING = 300;
  received_heading.DESTINATION_HEADING = 190;
  turn_towards_destination();
  TEST_ASSERT_EQUAL(heading_difference, 110);
  TEST_ASSERT_EQUAL(turn_direction, left);
}
