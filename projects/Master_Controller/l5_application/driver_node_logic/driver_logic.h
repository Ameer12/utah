#pragma once

#include "project.h"

typedef enum { right = 0, left = 1 } direction_t;
// This should copy the sensor data to its internal "static" struct instance of dbc_SENSOR_data_s
void driver__process_input(dbc_SENSOR_SONARS_ROUTINE_s *sensor_data);

void driver__process_geo_controller_directions(dbc_COMPASS_HEADING_DISTANCE_s *geo_heading);

// This should operate on the "static" struct instance of dbc_SENSOR_data_s to run the obstacle avoidance logic
dbc_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG_s driver__get_motor_commands(void);