#include "driver_logic.h"
#include "can_bus.h"
#include "project.h"
#include <math.h>
#include <stdbool.h>
#include <stdint.h>

static dbc_SENSOR_SONARS_ROUTINE_s internal_sensor_data; // should not be accessed outside of driver_logic

static dbc_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG_s internal_motor_data = {.DC_MOTOR_DRIVE_SPEED_sig = 10,
                                                                     .SERVO_STEER_ANGLE_sig = 0};

static dbc_COMPASS_HEADING_DISTANCE_s received_heading;

static uint8_t distance_for_object = 25;

static const int max_angle_threshold = 45;

static const int offset_to_angle = 5;

static const int reverse_ideal_speed = -3;

static const int ideal_speed_object_detected = 5;

static float heading_difference = 0;

static direction_t turn_direction;

static void change_angle_of_car(bool is_obstactle_on_right) {
  if (is_obstactle_on_right == true) {
    internal_motor_data.SERVO_STEER_ANGLE_sig = (internal_motor_data.SERVO_STEER_ANGLE_sig >= -40)
                                                    ? internal_motor_data.SERVO_STEER_ANGLE_sig - offset_to_angle
                                                    : -max_angle_threshold;
  } else {
    internal_motor_data.SERVO_STEER_ANGLE_sig = (internal_motor_data.SERVO_STEER_ANGLE_sig <= 40)
                                                    ? internal_motor_data.SERVO_STEER_ANGLE_sig + offset_to_angle
                                                    : max_angle_threshold;
  }
  internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig = ideal_speed_object_detected;
}

static void forward_drive(void) {
  internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig = (internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig < 5)
                                                     ? internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig + offset_to_angle
                                                     : 10;
}

static void set_ideal_forward(void) {
  // internal_motor_data.SERVO_STEER_ANGLE_sig = 0;
  internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig = 5; // max speed
}

static void reverse_car_and_turn(void) {
  internal_motor_data.SERVO_STEER_ANGLE_sig = (internal_motor_data.SERVO_STEER_ANGLE_sig <= 35)
                                                  ? internal_motor_data.SERVO_STEER_ANGLE_sig + 10
                                                  : max_angle_threshold;
  internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig = reverse_ideal_speed;
}

static bool check_whether_obstacle_detected(void) {
  if (internal_sensor_data.SENSOR_SONARS_left <= distance_for_object ||
      internal_sensor_data.SENSOR_SONARS_middle <= distance_for_object ||
      internal_sensor_data.SENSOR_SONARS_right <= distance_for_object ||
      internal_sensor_data.SENSOR_SONARS_rear <= distance_for_object) {
    return true;
  }
  return false;
}
static void tell_motors_to_move(direction_t direction_to_turn, float turn_magnitude, float distance_magnitude) {

  // Direction to the right or left just means your angle change will be positive or negative
  if (direction_to_turn == right) {
    // 45 is the maxiumum degree that the wheels can turn in any one direction but turn_magnitude
    // can be at maxiumum 180 beacuse of how the heading difference is calculated
    // 180/4 makes the turn magnitude comparable to this range (180/9 also works for less of these if cases)
    //[-45][45]
    if (turn_magnitude / 4 >= 40) {
      internal_motor_data.SERVO_STEER_ANGLE_sig = 40;
    } else if (turn_magnitude / 4 >= 35) {
      internal_motor_data.SERVO_STEER_ANGLE_sig = 35;
    } else if (turn_magnitude / 4 >= 30) {
      internal_motor_data.SERVO_STEER_ANGLE_sig = 30;
    } else if (turn_magnitude / 4 >= 25) {
      internal_motor_data.SERVO_STEER_ANGLE_sig = 25;
    } else if (turn_magnitude / 4 >= 20) {
      internal_motor_data.SERVO_STEER_ANGLE_sig = 20;
    } else if (turn_magnitude / 4 >= 15) {
      internal_motor_data.SERVO_STEER_ANGLE_sig = 15;
    } else if (turn_magnitude / 4 >= 10) {
      internal_motor_data.SERVO_STEER_ANGLE_sig = 10;
    } else if (turn_magnitude / 4 >= 5) {
      internal_motor_data.SERVO_STEER_ANGLE_sig = 5;
    } else {
      // At an angle difference this small, you would probably stop making adjustments,
      // this can be "higher up" in the if chain
    }
  } else {
    // Do the same thing but the angles are negative
    if (turn_magnitude / 4 >= 40) {
      internal_motor_data.SERVO_STEER_ANGLE_sig = -40;
    } else if (turn_magnitude / 4 >= 35) {
      internal_motor_data.SERVO_STEER_ANGLE_sig = -35;
    } else if (turn_magnitude / 4 >= 30) {
      internal_motor_data.SERVO_STEER_ANGLE_sig = -30;
    } else if (turn_magnitude / 4 >= 25) {
      internal_motor_data.SERVO_STEER_ANGLE_sig = -25;
    } else if (turn_magnitude / 4 >= 20) {
      internal_motor_data.SERVO_STEER_ANGLE_sig = -20;
    } else if (turn_magnitude / 4 >= 15) {
      internal_motor_data.SERVO_STEER_ANGLE_sig = -15;
    } else if (turn_magnitude / 4 >= 10) {
      internal_motor_data.SERVO_STEER_ANGLE_sig = -10;
    } else if (turn_magnitude / 4 >= 5) {
      internal_motor_data.SERVO_STEER_ANGLE_sig = -5;
    } else {
    }
  }
  // distance can be anything but speed is limited to -10 to 10
  // What ratio of distance:speed would give the best change
  // distance is in meters
  if (distance_magnitude >= 100) {
    internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig = 7;
  } else if (distance_magnitude >= 50) {
    internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig = 5;
  } else if (distance_magnitude >= 25) {
    internal_motor_data.DC_MOTOR_DRIVE_SPEED_sig = 3;
  } else {
  }
}
static void turn_towards_destination(void) {
  turn_direction = right;
  heading_difference = received_heading.DESTINATION_HEADING - received_heading.CURRENT_HEADING;

  if (heading_difference > 180) {
    turn_direction = left;
    heading_difference = 360 - heading_difference;
  } else if (heading_difference < 0 && heading_difference > -180) {
    turn_direction = left;
    heading_difference = fabs(heading_difference);
  } else if (heading_difference < -180) {
    turn_direction = right;
    heading_difference = fabs(360 + heading_difference);
  } else if (heading_difference > 0 && heading_difference <= 180) {
    turn_direction = right;
  }
  tell_motors_to_move(turn_direction, heading_difference, received_heading.DISTANCE);
}

void driver__process_input(dbc_SENSOR_SONARS_ROUTINE_s *sensor_data) {
  internal_sensor_data.SENSOR_SONARS_left =
      sensor_data->SENSOR_SONARS_left; // deep copy in case data in during processing
  internal_sensor_data.SENSOR_SONARS_right = sensor_data->SENSOR_SONARS_right;
  internal_sensor_data.SENSOR_SONARS_middle = sensor_data->SENSOR_SONARS_middle;
  internal_sensor_data.SENSOR_SONARS_rear = sensor_data->SENSOR_SONARS_rear;
}
void driver__process_geo_controller_directions(dbc_COMPASS_HEADING_DISTANCE_s *geo_heading) {
  received_heading.CURRENT_HEADING = geo_heading->CURRENT_HEADING;
  received_heading.DESTINATION_HEADING = geo_heading->DESTINATION_HEADING;
  received_heading.DISTANCE = geo_heading->DISTANCE;
}
dbc_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG_s driver__get_motor_commands(void) {
  bool is_object_on_right = false;
  if (check_whether_obstacle_detected()) {
    if (internal_sensor_data.SENSOR_SONARS_left <= distance_for_object &&
        internal_sensor_data.SENSOR_SONARS_middle <= distance_for_object &&
        internal_sensor_data.SENSOR_SONARS_right <= distance_for_object) {
      reverse_car_and_turn();
    } else if ((internal_sensor_data.SENSOR_SONARS_left <= distance_for_object &&
                internal_sensor_data.SENSOR_SONARS_middle <= distance_for_object) ||
               internal_sensor_data.SENSOR_SONARS_left <= distance_for_object) {
      is_object_on_right = false;
      change_angle_of_car(is_object_on_right);
    } else if ((internal_sensor_data.SENSOR_SONARS_right <= distance_for_object &&
                internal_sensor_data.SENSOR_SONARS_middle <= distance_for_object) ||
               internal_sensor_data.SENSOR_SONARS_right <= distance_for_object) {
      is_object_on_right = true;
      change_angle_of_car(is_object_on_right);
    } else if (internal_sensor_data.SENSOR_SONARS_rear <= distance_for_object) {
      is_object_on_right =
          (internal_sensor_data.SENSOR_SONARS_right < internal_sensor_data.SENSOR_SONARS_left) ? true : false;
      change_angle_of_car(is_object_on_right);
      forward_drive();
    } else if (internal_sensor_data.SENSOR_SONARS_middle <= distance_for_object) {
      is_object_on_right =
          (internal_sensor_data.SENSOR_SONARS_right < internal_sensor_data.SENSOR_SONARS_left) ? true : false;
      // change_angle_of_car(is_object_on_right);
      // forward_drive();
      reverse_car_and_turn();
    } else {
      // do nothing, should not reach this point, object is detected case should be handled
    }
  } else {
    // Drive towards what geo points me to
    turn_towards_destination();
    set_ideal_forward();
  }
  return internal_motor_data;
}
