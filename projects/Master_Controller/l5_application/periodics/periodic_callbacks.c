#include "periodic_callbacks.h"
#include "board_io.h"
#include "can_bus_initializer.h"
#include "can_module.h"
#include "gpio.h"

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) { can_bus_initializer(); }

void periodic_callbacks__1Hz(uint32_t callback_count) {
  gpio__toggle(board_io__get_led0());
  // periodic_callbacks_1Hz_master_commands();
  // Add your code here
}

void periodic_callbacks__10Hz(uint32_t callback_count) {
  // gpio__toggle(board_io__get_led1());
  if (callback_count % 5) {
    can_bus_handler__process_all_geo_messages();
  } else {
  }
  // Add your code here
}
void periodic_callbacks__100Hz(uint32_t callback_count) {
  // gpio__toggle(board_io__get_led2());
  if (callback_count % 5) {
    can_bus_handler__process_all_sensor_messages();
    periodic_callbacks_10Hz_motor_commands();
  } else {
  }
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {
  gpio__toggle(board_io__get_led3());
  // Add your code here
}
