#include "can_module.h"
#include "can_bus.h"
#include "driver_logic.h"
#include "project.h"
#include <stdio.h>

static dbc_DBG_DRIVER_CAN_STATUS_s get_can_bus_status(void) {
  dbc_DBG_DRIVER_CAN_STATUS_s can_bus_status;
  can_bus_status.DBG_CAN_INIT_STATUS = !(can__is_bus_off(can1));
  can_bus_status.DBG_CAN_RX_DROP_COUNT = can__get_rx_dropped_count(can1);
  return can_bus_status;
}

void can_bus_handler__process_all_sensor_messages(void) {
  can__msg_t can_msg = {0};
  dbc_SENSOR_SONARS_ROUTINE_s sensor_data = {};

  // Receive all messages
  while (can__rx(can1, &can_msg, 0)) {
    const dbc_message_header_t header = {
        .message_id = can_msg.msg_id,
        .message_dlc = can_msg.frame_fields.data_len,
    };

    // If sensor data was successfully decoded, hand it over to the driver logic
    if (dbc_decode_SENSOR_SONARS_ROUTINE(&sensor_data, header, can_msg.data.bytes)) {
      driver__process_input(&sensor_data);
    }
  }
}

void can_bus_handler__process_all_geo_messages(void) {
  can__msg_t can_msg = {0};
  dbc_COMPASS_HEADING_DISTANCE_s geo_data = {};

  // Receive all messages
  while (can__rx(can1, &can_msg, 0)) {
    const dbc_message_header_t header = {
        .message_id = can_msg.msg_id,
        .message_dlc = can_msg.frame_fields.data_len,
    };

    // If compass data was successfully decoded, hand it over to the driver logic
    if (dbc_decode_COMPASS_HEADING_DISTANCE(&geo_data, header, can_msg.data.bytes)) {
      driver__process_geo_controller_directions(&geo_data);
    }
  }
}

void periodic_callbacks_10Hz_motor_commands(void) {
  const dbc_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG_s command = driver__get_motor_commands();

  can__msg_t can_msg = {};
  const dbc_message_header_t header = dbc_encode_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG(can_msg.data.bytes, &command);
  can_msg.msg_id = header.message_id;
  can_msg.frame_fields.data_len = header.message_dlc;
  can__tx(can1, &can_msg, 0);
}

// void can_bus_handler__manage_mia_10hz(void) {
//   // We are in 10hz slot, so increment MIA counter by 100ms
//   const uint32_t mia_increment_value = 100;
//   const dbc_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG_s mia_command = {0};

//   if (dbc_service_mia_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG(&mia_command, mia_increment_value)) {
//     printf("Driver is MIA");
//   }
// }

void periodic_callbacks_1Hz_master_commands(void) {
  const dbc_DBG_DRIVER_CAN_STATUS_s driver_can_status = get_can_bus_status();
  can__msg_t can_msg = {};
  const dbc_message_header_t driver_can_status_header =
      dbc_encode_DBG_DRIVER_CAN_STATUS(can_msg.data.bytes, &driver_can_status);
  can_msg.msg_id = driver_can_status_header.message_id;
  can_msg.frame_fields.data_len = driver_can_status_header.message_dlc;
  can__tx(can1, &can_msg, 0);
}
