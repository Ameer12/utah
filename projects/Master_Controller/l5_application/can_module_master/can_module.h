#pragma once

#include "can_bus.h"

// Invoke this method in your periodic callbacks
void can_bus_handler__process_all_geo_messages(void);
void can_bus_handler__process_all_sensor_messages(void);
void periodic_callbacks_10Hz_motor_commands(void);

void can_bus_handler__manage_mia_10hz(void);
void periodic_callbacks_1Hz_master_commands(void);