#include <stdio.h>
#include <string.h>

#include "Mockcompass.h"
#include "Mockgpio.h"
#include "Mockgps.h"
#include "geo_logic.c"
#include "unity.h"

void setUp(void) {
  for (int i = 0; i < 5; i++) {
    is_GPS_destination_set[i] = false;
  }
}
void test_determine_destination_location_arrived(void) {
  gps_coordinates_t current_location = {37.51, 125.91, 40};
  internal_geo_destination_data[0].DEST_LATITUDE = 37.51;
  internal_geo_destination_data[0].DEST_LONGITUDE = 125.91;
  is_GPS_destination_set[0] = true;
  internal_geo_destination_data[1].DEST_LATITUDE = 40.01;
  internal_geo_destination_data[1].DEST_LONGITUDE = 131.91;
  is_GPS_destination_set[1] = true;
  location_of_shortest_distance_in_array = 0;
  current_destination.CURRENT_DEST_LATITUDE = 37.51;
  current_destination.CURRENT_DEST_LONGITUDE = 125.91;
  current_RC_CAR_location.GPS_CURRENT_LAT = 37.51;
  current_RC_CAR_location.GPS_CURRENT_LONG = 125.91;

  determine_destination_location();
  TEST_ASSERT_EQUAL(current_destination.CURRENT_DEST_LATITUDE, 40.01);
  TEST_ASSERT_EQUAL(current_destination.CURRENT_DEST_LONGITUDE, 131.91);
}

void test_determine_destination_location_not_arrived(void) {
  gps_coordinates_t current_location = {37.51, 125.91, 40};
  internal_geo_destination_data[0].DEST_LATITUDE = 37.51;
  internal_geo_destination_data[0].DEST_LONGITUDE = 125.91;
  is_GPS_destination_set[0] = true;
  internal_geo_destination_data[1].DEST_LATITUDE = 40.01;
  internal_geo_destination_data[1].DEST_LONGITUDE = 131.91;
  is_GPS_destination_set[1] = true;
  location_of_shortest_distance_in_array = 0;
  current_destination.CURRENT_DEST_LATITUDE = 37.51;
  current_destination.CURRENT_DEST_LONGITUDE = 125.91;
  current_RC_CAR_location.GPS_CURRENT_LAT = 36.78;
  current_RC_CAR_location.GPS_CURRENT_LONG = 123.91;
  determine_destination_location();
  TEST_ASSERT_EQUAL(location_of_shortest_distance_in_array, 0);
  TEST_ASSERT_EQUAL(current_destination.CURRENT_DEST_LATITUDE, 37.51);
  TEST_ASSERT_EQUAL(current_destination.CURRENT_DEST_LONGITUDE, 125.91);
}

void test_determine_destination_if_one_destination_not_valid(void) {
  gps_coordinates_t current_location = {37.51, 125.91, 40};
  internal_geo_destination_data[0].DEST_LATITUDE = 37.51;
  internal_geo_destination_data[0].DEST_LONGITUDE = 125.91;
  internal_geo_destination_data[1].DEST_LATITUDE = 40.01;
  internal_geo_destination_data[1].DEST_LONGITUDE = 131.91;
  is_GPS_destination_set[1] = true;
  location_of_shortest_distance_in_array = 0;
  current_destination.CURRENT_DEST_LATITUDE = 37.51;
  current_destination.CURRENT_DEST_LONGITUDE = 125.91;
  current_RC_CAR_location.GPS_CURRENT_LAT = 36.78;
  current_RC_CAR_location.GPS_CURRENT_LONG = 123.91;
  determine_destination_location();
  TEST_ASSERT_EQUAL(current_destination.CURRENT_DEST_LATITUDE, 40.01);
  TEST_ASSERT_EQUAL(current_destination.CURRENT_DEST_LONGITUDE, 131.91);
}

void test_check_geo_controller_process_single_geo_data_from_bridge(void) {
  dbc_GPS_DESTINATION_LOCATION_s geo_data;
  geo_data.DEST_LATITUDE = 90;
  geo_data.DEST_LONGITUDE = -100;
  geo_controller_process_GEO_data(&geo_data);
  TEST_ASSERT_EQUAL(internal_geo_destination_data[0].DEST_LATITUDE, geo_data.DEST_LATITUDE);
  TEST_ASSERT_EQUAL(internal_geo_destination_data[0].DEST_LONGITUDE, geo_data.DEST_LONGITUDE);
}

void test_check_geo_controller_process_GEO_date_from_bridge(void) {
  dbc_GPS_DESTINATION_LOCATION_s geo_data;
  geo_data.DEST_LATITUDE = 30;
  geo_data.DEST_LONGITUDE = -100;
  for (int i = 0; i < 5; i++) {
    geo_controller_process_GEO_data(&geo_data);
    geo_data.DEST_LATITUDE += 5;
    geo_data.DEST_LONGITUDE -= .5;
  }
  geo_data.DEST_LATITUDE = 30;
  geo_data.DEST_LONGITUDE = -100;
  for (int i = 0; i < 5; i++) {
    TEST_ASSERT_EQUAL(internal_geo_destination_data[i].DEST_LATITUDE, geo_data.DEST_LATITUDE);
    TEST_ASSERT_EQUAL(internal_geo_destination_data[i].DEST_LONGITUDE, geo_data.DEST_LONGITUDE);
    geo_data.DEST_LATITUDE += 5;
    geo_data.DEST_LONGITUDE -= .5;
  }
}

void check_geo_controller_add_at_spot_3(void) {
  test_check_geo_controller_process_GEO_date_from_bridge();
  is_GPS_destination_set[3] = false;
  dbc_GPS_DESTINATION_LOCATION_s geo_data;
  geo_data.DEST_LATITUDE = 71;
  geo_data.DEST_LONGITUDE = -91.45;
  geo_controller_process_GEO_data(&geo_data);
  TEST_ASSERT_EQUAL(is_GPS_destination_set[3], true);
  TEST_ASSERT_EQUAL_FLOAT(internal_geo_destination_data[3].DEST_LATITUDE, geo_data.DEST_LATITUDE);
  TEST_ASSERT_EQUAL_FLOAT(internal_geo_destination_data[3].DEST_LONGITUDE, geo_data.DEST_LONGITUDE);
}

void test_calculate_destination_bearing(void) {
  float expected_bearing = 49.15259;
  current_destination.CURRENT_DEST_LATITUDE = 37.3673;
  current_destination.CURRENT_DEST_LONGITUDE = -121.8914;
  current_RC_CAR_location.GPS_CURRENT_LAT = 37.3564;
  current_RC_CAR_location.GPS_CURRENT_LONG = -121.9047;
  float data = calculate_destination_bearing();
  TEST_ASSERT_EQUAL_FLOAT(expected_bearing, data);
}

void test_calculate_destination_bearing_2nd_coordinate(void) {
  float expected_bearing = 5.819368;
  current_destination.CURRENT_DEST_LATITUDE = 37.3673;
  current_destination.CURRENT_DEST_LONGITUDE = -121.8914;
  current_RC_CAR_location.GPS_CURRENT_LAT = 37.34033;
  current_RC_CAR_location.GPS_CURRENT_LONG = -121.89431;
  float data = calculate_destination_bearing();
  TEST_ASSERT_EQUAL_FLOAT(expected_bearing, data);
}

void test_determine_compass_heading_and_distance(void) {
  gps_coordinates_t gps_peripheral;
  float compass_heading;
  float calculated_distance = 1688.165;
  current_destination.CURRENT_DEST_LATITUDE = 37.3673;
  current_destination.CURRENT_DEST_LONGITUDE = -121.8914;
  current_RC_CAR_location.GPS_CURRENT_LAT = 37.3564;
  current_RC_CAR_location.GPS_CURRENT_LONG = -121.9047;
  gps__get_coordinates_ExpectAndReturn(gps_peripheral);
  compass__run_once_get_heading_ExpectAndReturn(compass_heading);
  determine_compass_heading_and_distance();
  TEST_ASSERT_EQUAL_FLOAT(calculated_distance, current_destination_information.DISTANCE);
  gps_coordinates_t gps_peripheral_data;
}

void test_determine_compass_heading_and_distance_2(void) {
  gps_coordinates_t gps_peripheral;
  float calculated_distance = 3009.903;
  float compass_heading;
  current_destination.CURRENT_DEST_LATITUDE = 37.3673;
  current_destination.CURRENT_DEST_LONGITUDE = -121.8914;
  current_RC_CAR_location.GPS_CURRENT_LAT = 37.34033;
  current_RC_CAR_location.GPS_CURRENT_LONG = -121.89431;
  gps__get_coordinates_ExpectAndReturn(gps_peripheral);
  compass__run_once_get_heading_ExpectAndReturn(compass_heading);
  determine_compass_heading_and_distance();
  TEST_ASSERT_EQUAL_FLOAT(calculated_distance, current_destination_information.DISTANCE);
  gps_coordinates_t gps_peripheral_data;
}

void test_calculate_shortest_distance(void) {
  int array_spot_for_shortest_distance = 0;
  internal_geo_destination_data[0].DEST_LATITUDE = 37.51;
  internal_geo_destination_data[0].DEST_LONGITUDE = 125.91;
  is_GPS_destination_set[0] = true;
  internal_geo_destination_data[1].DEST_LATITUDE = 40.01;
  internal_geo_destination_data[1].DEST_LONGITUDE = 131.91;
  is_GPS_destination_set[1] = true;
  internal_geo_destination_data[2].DEST_LATITUDE = 48.01;
  internal_geo_destination_data[2].DEST_LONGITUDE = 51.91;
  is_GPS_destination_set[2] = true;

  current_RC_CAR_location.GPS_CURRENT_LAT = 37.34033;
  current_RC_CAR_location.GPS_CURRENT_LONG = 121.89431;
  calculate_shortest_distance();
  TEST_ASSERT_EQUAL(array_spot_for_shortest_distance, location_of_shortest_distance_in_array);
}

void test_calculate_shortest_distance_2(void) {
  int array_spot_for_shortest_distance = 1;
  is_GPS_destination_set[0] = false;
  is_GPS_destination_set[1] = true;
  is_GPS_destination_set[2] = true;
  // update rc car to where destination 0 was
  current_RC_CAR_location.GPS_CURRENT_LAT = 37.51;
  current_RC_CAR_location.GPS_CURRENT_LONG = 125.91;
  calculate_shortest_distance();
  TEST_ASSERT_EQUAL(array_spot_for_shortest_distance, location_of_shortest_distance_in_array);
}

void test_calculate_shortest_distance_3(void) {
  test_calculate_shortest_distance_2();
  int array_spot_for_shortest_distance = 2;
  is_GPS_destination_set[0] = false;
  is_GPS_destination_set[1] = false;
  is_GPS_destination_set[2] = true;
  // update rc car to where destination 0 was
  current_RC_CAR_location.GPS_CURRENT_LAT = 40.01;
  current_RC_CAR_location.GPS_CURRENT_LONG = 131.91;
  calculate_shortest_distance();
  TEST_ASSERT_EQUAL(array_spot_for_shortest_distance, location_of_shortest_distance_in_array);
}