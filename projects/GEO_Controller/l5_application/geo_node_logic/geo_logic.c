/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Main Module Header */
#include "geo_logic.h"
/* Standard Includes */
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
/* External Includes */

#include "compass.h"
#include "ff.h"
#include "gpio.h"
#include "gps.h"
#include "project.h"
#include "waypoints.h"
/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/

#define PI 3.14159265358979323846
#define coordinate_array_size_max 5U
/***********************************************************************************************************************
 *
 *                                                  T Y P E D E F S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                             P R I V A T E   F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/
static const uint8_t max_desinations_allowed = 5U;

// logic add destination
// determine shortest distance
// once we have arrived, set destination as invalid

static dbc_GPS_DESTINATION_LOCATION_s internal_geo_destination_data[coordinate_array_size_max];

static dbc_GPS_CURRENT_DESTINATIONS_DATA_s current_destination = {0U};

static dbc_GPS_CURRENT_INFO_s current_RC_CAR_location = {0U};

static dbc_COMPASS_HEADING_DISTANCE_s current_destination_information = {0U};

static confirm_received_destination_s last_added_destination = {0U};

static bool is_GPS_destination_set[coordinate_array_size_max] = {false};

static int location_of_shortest_distance_in_array = -1;

static int distance;

static gps_coordinates_t rc_car, destination;
/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                         P R I V A T E   F U N C T I O N S
 *
 **********************************************************************************************************************/

static bool check_have_we_arrived(void) {
  float margin_of_error = 0.00002;
  const float difference_in_longitude =
      current_destination.CURRENT_DEST_LONGITUDE - current_RC_CAR_location.GPS_CURRENT_LONG;
  const float difference_in_latitude =
      current_destination.CURRENT_DEST_LATITUDE - current_RC_CAR_location.GPS_CURRENT_LAT;
  float current_distance_to_location = sqrt(pow(difference_in_longitude, 2) + pow(difference_in_latitude, 2));
  if (current_distance_to_location < margin_of_error) {
    is_GPS_destination_set[location_of_shortest_distance_in_array] = false;
    return true;
  }
  return false;
}

static bool check_if_location_set(void) {
  bool status = false;
  for (int i = 0; i < coordinate_array_size_max; i++) {
    if (is_GPS_destination_set[i] == true) {
      status = true;
    }
  }
  return status;
}
static void write_file_using_fatfs_pi(float value[], const char *filename) {
  FIL file; // File handle
  UINT bytes_written = 0;
  FRESULT result = f_open(&file, filename, (FA_WRITE | FA_OPEN_APPEND));

  if (FR_OK == result) {
    char string[100];
    sprintf(string, "%f, %f, %f\n", value[0], value[1], value[2]);
    if (FR_OK == f_write(&file, string, strlen(string), &bytes_written)) {
    } else {
      printf("ERROR: Failed to write data to file\n");
    }
    f_close(&file);
  } else {
    printf("ERROR: Failed to open: %s\n", filename);
  }
}

static float calculate_destination_bearing(void) {
  // setting variables
  const float difference_in_longitude =
      current_destination.CURRENT_DEST_LONGITUDE - current_RC_CAR_location.GPS_CURRENT_LONG;
  const float destination_latitude = current_destination.CURRENT_DEST_LATITUDE;
  const float source_latitude = current_RC_CAR_location.GPS_CURRENT_LAT;

  // Actual calculations
  float x = cos(destination_latitude) * sin(difference_in_longitude);
  float y = cos(source_latitude) * sin(destination_latitude) -
            sin(source_latitude) * cos(destination_latitude) * cos(difference_in_longitude);
  float bearing = (float)(atan2(x, y));
  if (bearing < 0) {
    return ((bearing * (180.0f / (float)PI)) + 360);
  }
  // printf("BEARING IN RADIANS: %f\n", (bearing * (180.0f / (float)PI)));
  return (bearing * (180.0f / (float)PI));
}

void write_to_sd_card(void) {
  const char *filename = "compass_and_bearing.csv";
  float arr[3];
  arr[0] = get_current_compass_heading_value();
  arr[1] = calculate_destination_bearing();
  arr[2] = get_distance();
  write_file_using_fatfs_pi(arr, filename);
}

// static float calculate_distance_rc_car_to_destination(void) {
//   return sqrt(pow(current_RC_CAR_location.GPS_CURRENT_LONG - current_destination.CURRENT_DEST_LONGITUDE, 2) +
//               pow(current_RC_CAR_location.GPS_CURRENT_LAT - current_destination.CURRENT_DEST_LATITUDE, 2));
// }

static float calculate_distance_rc_car_to_destination_in_meters(void) {
  const int Earth_Radius_in_km = 6371;
  const float delta_lat =
      (current_RC_CAR_location.GPS_CURRENT_LAT - current_destination.CURRENT_DEST_LATITUDE) * (PI / 180);
  const float delta_long =
      (current_RC_CAR_location.GPS_CURRENT_LONG - current_destination.CURRENT_DEST_LONGITUDE) * (PI / 180);
  float a = pow(sinf(delta_lat / 2), 2) + cosf(current_destination.CURRENT_DEST_LATITUDE * (PI / 180)) *
                                              cosf(current_RC_CAR_location.GPS_CURRENT_LAT * (PI / 180)) *
                                              pow(sin(delta_long / 2), 2);
  float c = 2 * atan2f(sqrt(a), sqrt(1 - a));
  distance = (float)(Earth_Radius_in_km * c * 1000);
  return (float)(Earth_Radius_in_km * c * 1000);
}

static float calculate_distance(uint8_t array_index) {
  return sqrt(
      pow(current_RC_CAR_location.GPS_CURRENT_LONG - internal_geo_destination_data[array_index].DEST_LONGITUDE, 2) +
      pow(current_RC_CAR_location.GPS_CURRENT_LAT - internal_geo_destination_data[array_index].DEST_LATITUDE, 2));
}

static void calculate_shortest_distance(void) {
  // longtitude is X, latitude is y
  float shortest_distance = 1E+37;
  float calculated_distance;
  location_of_shortest_distance_in_array = -1; // set as invalid
  for (int i = 0; i < max_desinations_allowed; i++) {
    if (is_GPS_destination_set[i] == true) {
      calculated_distance = calculate_distance(i);
      if (calculated_distance < shortest_distance) {
        location_of_shortest_distance_in_array = i;
        shortest_distance = calculated_distance;
      }
    }
  }
}

/***********************************************************************************************************************
 *
 *                                          P U B L I C   F U N C T I O N S
 *
 **********************************************************************************************************************/

dbc_GPS_CURRENT_INFO_s geo_controller_process_GEO_current_location(void) {
  gps_coordinates_t gps_peripheral_data;
  gps_peripheral_data = gps__get_coordinates();
  current_RC_CAR_location.GPS_CURRENT_LONG = gps_peripheral_data.longitude;
  current_RC_CAR_location.GPS_CURRENT_LAT = gps_peripheral_data.latitude;
  return current_RC_CAR_location;
}

void geo_controller_process_GEO_data(dbc_GPS_DESTINATION_LOCATION_s *geo_data) {
  for (int i = 0; i < max_desinations_allowed; i++) {
    if (is_GPS_destination_set[i] == false) {
      internal_geo_destination_data[i].DEST_LATITUDE = geo_data->DEST_LATITUDE;
      internal_geo_destination_data[i].DEST_LONGITUDE = geo_data->DEST_LONGITUDE;
      last_added_destination.status_values.RECEIVED_DEST_LATITUDE = geo_data->DEST_LATITUDE;
      last_added_destination.status_values.RECEIVED_DEST_LONGITUDE = geo_data->DEST_LONGITUDE;
      is_GPS_destination_set[i] = true;
      last_added_destination.is_data_pending = true;
      break;
    }
  }
}

dbc_GPS_CURRENT_DESTINATIONS_DATA_s determine_destination_location(void) {
  check_have_we_arrived(); // change this to check
  calculate_shortest_distance();
  current_destination.CURRENT_DEST_LATITUDE =
      internal_geo_destination_data[location_of_shortest_distance_in_array].DEST_LATITUDE;
  current_destination.CURRENT_DEST_LONGITUDE =
      internal_geo_destination_data[location_of_shortest_distance_in_array].DEST_LONGITUDE;
  rc_car.latitude = current_RC_CAR_location.GPS_CURRENT_LAT;
  rc_car.longitude = current_RC_CAR_location.GPS_CURRENT_LONG;
  destination.latitude = current_destination.CURRENT_DEST_LATITUDE;
  destination.longitude = current_destination.CURRENT_DEST_LONGITUDE;
  find_next_point(rc_car, destination);
  return current_destination;
}
int get_distance(void) { return distance; }
dbc_COMPASS_HEADING_DISTANCE_s determine_compass_heading_and_distance(void) {
  gps_coordinates_t gps_peripheral_data;
  gps_peripheral_data = gps__get_coordinates();
  // waypoint algorithm goes here
  find_next_point(rc_car, destination);
  current_destination_information.CURRENT_HEADING = compass__run_once_get_heading();
  // current_destination_information.DESTINATION_HEADING = calculate_destination_bearing();
  current_destination_information.DESTINATION_HEADING = get_current_bearing();
  current_destination_information.DISTANCE = calculate_distance_rc_car_to_destination_in_meters();
  if (check_have_we_arrived() == true || check_if_location_set() == false) {
    current_destination_information.CURRENT_HEADING = 0;
    current_destination_information.DESTINATION_HEADING = 0;
    current_destination_information.DISTANCE = 0;
  }
  return current_destination_information;
}

// DEBUG functions

dbc_GPS_COMPASS_STATUS_s check_GPS_Compass_Status(void) {
  dbc_GPS_COMPASS_STATUS_s status_values = {.COMPASS_LOCK_VALID = 0, .GPS_LOCK_VALID = 0};
  status_values.COMPASS_LOCK_VALID = compass__is_data_valid();
  status_values.GPS_LOCK_VALID = gps_is_fixed();
  return status_values;
}

void processed_compass_gps_data(void) { last_added_destination.is_data_pending = false; }

confirm_received_destination_s confirm_last_added_destination(void) { return last_added_destination; }

dbc_DBG_GPS_COMPASS_LOCK_LED_CHECK_s check_GPS_COMPASS_LED(void) {
  dbc_DBG_GPS_COMPASS_LOCK_LED_CHECK_s lock_status = {.COMPASS_LED_STATUS = 0, .GPS_LED_STATUS = 0};
  const gpio_s gps_fix = {0, 6};
  const gpio_s compass_fix = {0, 22};
  lock_status.COMPASS_LED_STATUS = !(gpio__get(compass_fix));
  lock_status.GPS_LED_STATUS = !(gpio__get(compass_fix));
  return lock_status;
}