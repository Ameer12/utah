#pragma once

/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Standard Includes */

#include <stdbool.h>
#include <stdint.h>
/* External Includes */

#include "project.h"
/* Module Includes */

/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                                  T Y P E D E F S
 *
 **********************************************************************************************************************/
typedef enum {
  compass_magnetomer_CRA_REG_M = 0x00,
  compass_magnetomer_CRB_REG_M = 0x01,
  compass_magnetomer_MR_REG_M = 0x02,
  compass_magnetomer_OUT_X_H_M = 0x03,
  compass_magnetomer_OUT_X_L_M = 0x04,
  compass_magnetomer_OUT_Z_H_M = 0x05,
  compass_magnetomer_OUT_Z_L_M = 0x06,
  compass_magnetomer_OUT_Y_H_M = 0x07,
  compass_magnetomer_OUT_Y_L_M = 0x08,
  compass_magnetomer_SR_REG_Mg = 0x09,
  compass_magnetomer_IRA_REG_M = 0x0A,
  compass_magnetomer_IRB_REG_M = 0x0B,
  compass_magnetomer_IRC_REG_M = 0x0C,
  compass_magnetomer_TEMP_OUT_H_M = 0x31,
  compass_magnetomer_TEMP_OUT_L_M = 0x32
} compass_magnetometer_register_e;

typedef enum {                               // DEFAULT    TYPE
  compass_accelerometer_CTRL_REG1_A = 0x20,  // 00000111   rw
  compass_accelerometer_CTRL_REG2_A = 0x21,  // 00000000   rw
  compass_accelerometer_CTRL_REG3_A = 0x22,  // 00000000   rw
  compass_accelerometer_CTRL_REG4_A = 0x23,  // 00000000   rw
  compass_accelerometer_CTRL_REG5_A = 0x24,  // 00000000   rw
  compass_accelerometer_CTRL_REG6_A = 0x25,  // 00000000   rw
  compass_accelerometer_REFERENCE_A = 0x26,  // 00000000   r
  compass_accelerometer_STATUS_REG_A = 0x27, // 00000000   r
  compass_accelerometer_OUT_X_L_A = 0x28,
  compass_accelerometer_OUT_X_H_A = 0x29,
  compass_accelerometer_OUT_Y_L_A = 0x2A,
  compass_accelerometer_OUT_Y_H_A = 0x2B,
  compass_accelerometer_OUT_Z_L_A = 0x2C,
  compass_accelerometer_OUT_Z_H_A = 0x2D,
  compass_accelerometer_FIFO_CTRL_REG_A = 0x2E,
  compass_accelerometer_FIFO_SRC_REG_A = 0x2F,
  compass_accelerometer_INT1_CFG_A = 0x30,
  compass_accelerometer_INT1_SOURCE_A = 0x31,
  compass_accelerometer_INT1_THS_A = 0x32,
  compass_accelerometer_INT1_DURATION_A = 0x33,
  compass_accelerometer_INT2_CFG_A = 0x34,
  compass_accelerometer_INT2_SOURCE_A = 0x35,
  compass_accelerometer_INT2_THS_A = 0x36,
  compass_accelerometer_INT2_DURATION_A = 0x37,
  compass_accelerometer_CLICK_CFG_A = 0x38,
  compass_accelerometer_CLICK_SRC_A = 0x39,
  compass_accelerometer_CLICK_THS_A = 0x3A,
  compass_accelerometer_TIME_LIMIT_A = 0x3B,
  compass_accelerometer_TIME_LATENCY_A = 0x3C,
  compass_accelerometer_TIME_WINDOW_A = 0x3D
} lsm303AccelRegisters_t;

typedef enum {
  magnetometer_gain_1_3 = 0x20,
  magnetometer_gain_1_9 = 0x40,
  magnetometer_gain_2_5 = 0x60,
  magnetometer_gain_4_0 = 0x80,
  magnetometer_gain_4_7 = 0xA0,
  magnetometer_gain_5_6 = 0xC0,
  magnetometer_gain_8_1 = 0xE0
} compass_magnetometer_gain_values_e;

typedef enum { magnetometer_read = 0x3D, magnetometer_write = 0x3C } compass_magnetometer_opcode_e;

typedef struct {
  float x;
  float y;
  float z;
} compass_data_t;

/***********************************************************************************************************************
 *
 *                                     F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

void compass__init(void);
float compass__run_once_get_heading(void);
void print_compass_run_once(void);
bool compass__is_data_valid(void);
float get_current_compass_heading_value(void);
void write_compass_to_sd(void);

// debug functions
dbc_DBG_RAW_COMPASS_DATA_s sent_RAW_COMPASS_REGISTER(void);
