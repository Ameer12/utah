/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Main Module Header */

#include "compass.h"
/* Standard Includes */

#include <math.h>
#include <stdio.h>
/* External Includes */

#include "board_io.h"
#include "clock.h"
#include "ff.h"
#include "gpio.h"
#include "i2c.h"

/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/

#define PI 3.14159265358979323846

/***********************************************************************************************************************
 *
 *                                                  T Y P E D E F S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                             P R I V A T E   F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/
static const i2c_e current_i2c = I2C__2;

static float current_compass_heading;

static compass_data_t magnetometer_processed = {0};

static compass_data_t accel_raw_data = {0};

static gpio_s compass_data_available = {GPIO__PORT_0, 22};

static gpio_s compass_data_available = {GPIO__PORT_0, 22};

/***********************************************************************************************************************
 *
 *                                         P R I V A T E   F U N C T I O N S
 *
 **********************************************************************************************************************/

static void write_file_using_fatfs_pi(float value, const char *filename) {
  FIL file; // File handle
  UINT bytes_written = 0;
  FRESULT result = f_open(&file, filename, (FA_WRITE | FA_OPEN_APPEND));

  if (FR_OK == result) {
    char string[64];
    sprintf(string, "%f\n", value);
    if (FR_OK == f_write(&file, string, strlen(string), &bytes_written)) {
    } else {
      printf("ERROR: Failed to write data to file\n");
    }
    f_close(&file);
  } else {
    printf("ERROR: Failed to open: %s\n", filename);
  }
}

static void transformation_accel(float uncalibrated_values[3]) {
  float calibrated_accel_values[3];
  float matrix[3][3] = {{1.009, .127, -.023}, {-.048, .976, -.003}, {.018, .053, 1.014}};
  float bias[3] = {-20.101, -32.401, 7.916};
  for (int i = 0; i < 3; ++i)
    uncalibrated_values[i] = uncalibrated_values[i] - bias[i];
  float result[3] = {0, 0, 0};
  for (int i = 0; i < 3; ++i)
    for (int j = 0; j < 3; ++j)
      result[i] += matrix[i][j] * uncalibrated_values[j];
  for (int i = 0; i < 3; ++i)
    calibrated_accel_values[i] = result[i];
  // accel_raw_data.x = calibrated_accel_values[0];
  // accel_raw_data.y = calibrated_accel_values[1];
  // accel_raw_data.z = calibrated_accel_values[2];
  accel_raw_data.x = uncalibrated_values[0];
  accel_raw_data.y = uncalibrated_values[1];
  accel_raw_data.z = uncalibrated_values[2];
}

static void transformation_mag(float uncalibrated_values[3]) {
  float calibrated_mag_values[3];
  float matrix[3][3] = {{1.167, .057, -.058}, {.02, 1.177, 0.02}, {.099, -.032, 1.234}};
  float bias[3] = {104.335, -100.174, -72.274};
  for (int i = 0; i < 3; ++i)
    uncalibrated_values[i] = uncalibrated_values[i] - bias[i];
  float result[3] = {0, 0, 0};
  for (int i = 0; i < 3; ++i)
    for (int j = 0; j < 3; ++j)
      result[i] += matrix[i][j] * uncalibrated_values[j];
  for (int i = 0; i < 3; ++i)
    calibrated_mag_values[i] = result[i];
  // magnetometer_processed.x = calibrated_mag_values[0];
  // magnetometer_processed.y = calibrated_mag_values[1];
  // magnetometer_processed.z = calibrated_mag_values[2];
  magnetometer_processed.x = uncalibrated_values[0];
  magnetometer_processed.y = uncalibrated_values[1];
  magnetometer_processed.z = uncalibrated_values[2];
}

static void get_raw_compass_data(void) {

  // https://appelsiini.net/2018/calibrate-magnetometer/
  uint8_t magnetometer_data[6] = {0U};
  uint8_t items_to_read = 6;
  float mag[3];
  i2c__read_slave_data(current_i2c, magnetometer_read, compass_magnetomer_OUT_X_H_M, magnetometer_data, items_to_read);

  mag[0] = (int16_t)(magnetometer_data[1] | (int16_t)(magnetometer_data[0] << 8));
  mag[2] = (int16_t)(magnetometer_data[3] | (int16_t)(magnetometer_data[2] << 8));
  mag[1] = (int16_t)(magnetometer_data[5] | (int16_t)(magnetometer_data[4] << 8));
  transformation_mag(mag);
}

static void get_raw_accelerometer_data(void) {
  uint8_t accel_data[6] = {0U};
  uint8_t items_to_read = 6;
  float accel[3];
  i2c__read_slave_data(current_i2c, 0x33, 0xA8, accel_data, items_to_read);

  accel[0] = (int16_t)((int16_t)accel_data[0] | (int16_t)(accel_data[1] << 8)) >> 4;
  accel[1] = (int16_t)((int16_t)accel_data[2] | (int16_t)(accel_data[3] << 8)) >> 4;
  accel[2] = (int16_t)((int16_t)accel_data[4] | (int16_t)(accel_data[5] << 8)) >> 4;
  transformation_accel(accel);
}
// Todo: accomodate for tilt for a more accurate reading
// static void get_current_compass_heading(void) {
//   const float processed_tesla_offset_y = magnetometer_processed.y;
//   const float processed_tesla_offset_x = magnetometer_processed.x;
//   current_compass_heading = atan2f(processed_tesla_offset_y, processed_tesla_offset_x) * (180 / PI);
//   if (current_compass_heading < 0) {
//     current_compass_heading += 360;
//   }
// }

// below with tilt-comp

static void get_current_compass_heading(void) {
  float alpha = 0.25; // alpha to create base for software optimizations
  static float Mxcnf;
  static float Mycnf;
  static float Mzcnf;
  static float Axcnf;
  static float Aycnf;
  static float Azcnf;

  float norm_m =
      sqrtf(magnetometer_processed.x * magnetometer_processed.x + magnetometer_processed.y * magnetometer_processed.y +
            magnetometer_processed.z * magnetometer_processed.z); // normalize data between all 3 axis
  float Mxz = magnetometer_processed.z / norm_m;
  float Mxy = magnetometer_processed.y / norm_m;
  float Mxx = magnetometer_processed.x / norm_m;
  float norm_a = sqrtf(accel_raw_data.x * accel_raw_data.x + accel_raw_data.y * accel_raw_data.y +
                       accel_raw_data.z * accel_raw_data.z);
  float Axz = accel_raw_data.z / norm_a;
  float Axy = accel_raw_data.y / norm_a;
  float Axx = accel_raw_data.x / norm_a;

  Mxcnf = Mxx * alpha + (Mxcnf * (1.0 - alpha));
  Mycnf = Mxy * alpha + (Mycnf * (1.0 - alpha));
  Mzcnf = Mxz * alpha + (Mzcnf * (1.0 - alpha));

  // Low-Pass filter accelerometer
  Axcnf = Axx * alpha + (Axcnf * (1.0 - alpha));
  Aycnf = Axy * alpha + (Aycnf * (1.0 - alpha));
  Azcnf = Axz * alpha + (Azcnf * (1.0 - alpha));

  float pitch = (double)asin((double)-Axcnf);
  float roll = (double)asin((double)Aycnf / cos((double)pitch));
  float Xh = Mxcnf * cos((double)pitch) + Mzcnf * sin((double)pitch);
  float Yh = Mxcnf * sin((double)roll) * sin((double)pitch) + Mycnf * cos((double)roll) -
             Mzcnf * sin((double)roll) * cos((double)pitch);
  current_compass_heading = (atan2(Yh, Xh)) * 180 / PI;
  current_compass_heading += 13;

  if (current_compass_heading < 0) {
    current_compass_heading = 360 + current_compass_heading;
  }
}
float get_current_compass_heading_value(void) { return current_compass_heading; }

void write_compass_to_sd(void) {
  const char *filename = "compass_data.csv";
  write_file_using_fatfs_pi(current_compass_heading, filename);
}

static void set_magnetometer_gain(compass_magnetometer_gain_values_e gain) {
  i2c__write_single(current_i2c, magnetometer_write, compass_magnetomer_CRB_REG_M, gain);
}
/***********************************************************************************************************************
 *
 *                                          P U B L I C   F U N C T I O N S
 *
 **********************************************************************************************************************/
void compass__init(void) {
  gpio__set_as_input(compass_data_available);
  gpio__enable_pull_down_resistors(compass_data_available);
  const uint8_t mag_register_value_setup[3] = {0x90, 0x00, 0x00};
  const uint32_t i2c_speed_hz = UINT32_C(400) * 1000;
  i2c__initialize(current_i2c, i2c_speed_hz, clock__get_peripheral_clock_hz());
  static int acceleration_address_write = 0x33;
  i2c__write_single(current_i2c, acceleration_address_write, 0x20, 0x47);
  i2c__write_single(current_i2c, acceleration_address_write, 0x23, 0x88);
  i2c__write_slave_data(current_i2c, magnetometer_write, compass_magnetomer_CRA_REG_M, mag_register_value_setup, 3);
  set_magnetometer_gain(magnetometer_gain_1_3);
}

bool compass__is_data_valid(void) {
  const bool data_ready = true;
  if (gpio__get(compass_data_available) == data_ready) {
    return true;
  } else {
    return false;
  }
}

float compass__run_once_get_heading(void) {
  get_raw_compass_data();
  get_raw_accelerometer_data();
  get_current_compass_heading();
  return current_compass_heading;
}

void print_compass_run_once(void) { fprintf(stderr, "COMPASS: %f\n", current_compass_heading); }

dbc_DBG_RAW_COMPASS_DATA_s sent_RAW_COMPASS_REGISTER(void) {
  dbc_DBG_RAW_COMPASS_DATA_s raw_data_dbc;
  raw_data_dbc.SIGNED_REGISTER_VAL_MAG_X = (int16_t)magnetometer_processed.x;
  raw_data_dbc.SIGNED_REGISTER_VAL_MAG_Y = (int16_t)magnetometer_processed.y;
  return raw_data_dbc;
}
