#include "waypoints.h"
#include "math.h"

#define PI 3.14159265358979323846
#define max_points 11U
/*
0i, 37.338722, -121.880646
1i, 37.338932, -121.880356
2i, 37.339001, -121.880531
3i, 37.339024, -121.880928
4i, 37.339130, -121.880829
5i, 37.339272, -121.880569
6i, 37.339401, -121.880470
7i, 37.339340, -121.880859
8i, 37.339321, -121.881020
9i, 37.339573, -121.880989
10i, 37.339729, -121.880905
11i, 37.339703, -121.881218
*/
// static const gps_coordinates_t locations_we_can_travel[max_points] = {
//     {37.338722, -121.880646, 0}, {37.338932, -121.880356, 0}, {37.339001, -121.880531, 0}, {37.339024, -121.880928,
//     0}, {37.339130, -121.880829, 0}, {37.339272, -121.880569, 0}, {37.339401, -121.880470, 0}, {37.339340,
//     -121.880859, 0}, {37.339321, -121.881020, 0}, {37.339573, -121.880989, 0}, {37.339729, -121.880905, 0},
//     {37.339703, -121.881218, 0}};

// 37.33908208809001, -121.88079296419474

static const gps_coordinates_t locations_we_can_travel[max_points] = {
    {37.33895, -121.88071, 0}, {37.33904, -121.88046, 0}, {37.33914, -121.88022, 0}, {37.339082, -121.88079, 0},
    {37.33917, -121.88085, 0}, {37.33925, -121.88061, 0}, {37.33935, -121.88037, 0}, {37.33937, -121.88101, 0},
    {37.33935, -121.88118, 0}, {37.33947, -121.88085, 0}, {37.33956, -121.88055, 0}};

static float coordinate_bearing;

static float calculate_distance_rc_car_to_destination_in_meters(gps_coordinates_t origin,
                                                                gps_coordinates_t destination) {
  const int Earth_Radius_in_km = 6371;
  const float delta_lat = (origin.latitude - destination.latitude) * (PI / 180);
  const float delta_long = (origin.longitude - destination.longitude) * (PI / 180);
  float a = pow(sinf(delta_lat / 2), 2) +
            cosf(destination.latitude * (PI / 180)) * cosf(origin.latitude * (PI / 180)) * pow(sin(delta_long / 2), 2);
  float c = 2 * atan2f(sqrt(a), sqrt(1 - a));
  return (float)(Earth_Radius_in_km * c * 1000);
}

static void set_bearing_for_waypoint(gps_coordinates_t origin, gps_coordinates_t destination) {
  // setting variables
  const float difference_in_longitude = destination.longitude - origin.longitude;
  const float destination_latitude = destination.latitude;
  const float source_latitude = origin.latitude;

  // Actual calculations
  float x = cos(destination_latitude) * sin(difference_in_longitude);
  float y = cos(source_latitude) * sin(destination_latitude) -
            sin(source_latitude) * cos(destination_latitude) * cos(difference_in_longitude);
  float bearing = (float)(atan2(x, y));
  if (bearing < 0) {
    coordinate_bearing = ((bearing * (180.0f / (float)PI)) + 360);
  } else {
    coordinate_bearing = (bearing * (180.0f / (float)PI));
  }
}

gps_coordinates_t find_next_point(gps_coordinates_t origin, gps_coordinates_t destination) {
  // 1) check distance from current to location save to destination distance
  // 2) go through waypoints, find closest waypoint first,
  //     - compare you distance to waypoint
  //     -compare distance from waypoint to destination(must be
  //      less than current destination distnace)
  // 3) repeat for all waypoints
  // 4) return best waypoint
  const float origin_to_destination_distance = calculate_distance_rc_car_to_destination_in_meters(origin, destination);
  float waypoint_distance_to_destination = 0;
  float origin_distance_to_waypoint = 0;
  float closest_way_point_disance = 1E+37;
  uint8_t waypoint_array_location = 0;

  for (uint8_t i = 0; i < max_points; i++) {
    origin_distance_to_waypoint =
        calculate_distance_rc_car_to_destination_in_meters(origin, locations_we_can_travel[i]);
    waypoint_distance_to_destination =
        calculate_distance_rc_car_to_destination_in_meters(locations_we_can_travel[i], destination);
    if ((origin_distance_to_waypoint <= closest_way_point_disance) &&
        (waypoint_distance_to_destination < origin_to_destination_distance)) {
      waypoint_array_location = i;
      closest_way_point_disance = origin_distance_to_waypoint;
    } else {
      // do nothing
    }
  }
  if (origin_to_destination_distance <= closest_way_point_disance) {
    set_bearing_for_waypoint(origin, destination);
    return destination;
  } else {
    set_bearing_for_waypoint(origin, locations_we_can_travel[waypoint_array_location]);
    return locations_we_can_travel[waypoint_array_location];
  }
}

float get_current_bearing(void) { return coordinate_bearing; }