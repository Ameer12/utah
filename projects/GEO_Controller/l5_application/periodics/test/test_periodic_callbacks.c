#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockcan_bus_initializer.h"
#include "Mockcan_module.h"
#include "Mockcompass.h"
#include "Mockgpio.h"
#include "Mockgps.h"

// Include the source we wish to test
#include "periodic_callbacks.h"

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  gps__init_Expect();
  compass__init_Expect();
  can_bus_initializer_ExpectAndReturn(true);
  gpio_s gpio = {};
  board_io__get_led0_ExpectAndReturn(gpio);
  board_io__get_led1_ExpectAndReturn(gpio);
  board_io__get_led2_ExpectAndReturn(gpio);
  board_io__get_led3_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  gpio__set_Expect(gpio);
  gpio__set_Expect(gpio);
  gpio__set_Expect(gpio);
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz(void) {
  periodic_callbacks_1Hz_GEO_commands_Expect();
  periodic_callbacks__1Hz(0);
}

void test_periodic_callbacks__10Hz(void) {
  for (uint32_t callback_count = 0; callback_count < 100; callback_count++) {
    gps__run_once_Expect();
    if (callback_count % 5 == 0) {
      periodic_callbacks_10Hz_GEO_commands_Expect();
    }
    if (callback_count % 10 == 0) {
      can_bus_handler__process_all_received_messages_Expect();
    }
    periodic_callbacks__10Hz(callback_count);
  }
}
