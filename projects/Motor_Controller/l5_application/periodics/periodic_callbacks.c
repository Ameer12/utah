/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Main Module Header */
#include "periodic_callbacks.h"

/* Standard Includes */
#include <stdio.h>

/* External Includes */
#include "board_io.h"
#include "gpio.h"

#include "can_bus_handler.h"
#include "dc_motor.h"
#include "motor_logic.h"
#include "rpm_sensor.h"
#include "servo_motor.h"

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/
#define arr_size 4
static const float speed_array[arr_size] = {0.0f, 10.0f, 0.0f, -10.0f};
static float target_drive_speed;
static float drive_speed;
static int8_t steer_angle;

bool self_test_done = false;

/***********************************************************************************************************************
 *
 *                                          P U B L I C   F U N C T I O N S
 *
 **********************************************************************************************************************/
/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) {
  // This method is invoked once when the periodic tasks are created
  can_bus_handler__init();
  dc_motor__init();
  servo_motor__init();
  rpm_sensor__init();

  gpio__set(board_io__get_led0());
  gpio__set(board_io__get_led1());
  gpio__set(board_io__get_led2());
  gpio__set(board_io__get_led3());

  next_drive_direction = DRIVE_NEUTRAL;
  current_drive_direction = DRIVE_NEUTRAL;
}

void periodic_callbacks__1Hz(uint32_t callback_count) {
  // Add your code here
  static uint32_t speed_index;

  if (!self_test_done) {
    self_test_done = motor_logic__self_test();
  } else {

    if (can_bus_handler__reset_can_bus_if_bus_goes_off()) {
      gpio__toggle(board_io__get_led0());
    }

    if (!(callback_count % 5)) {
      target_drive_speed = speed_array[speed_index]; // for testing
      speed_index = (speed_index + 1) % arr_size;
      if (can_bus_handler__motor_heartbeat_tx()) {
        gpio__toggle(board_io__get_led1());
      }
      if (can_bus_handler__motor_speed_tx(rpm_sensor__get_speed())) {
        gpio__toggle(board_io__get_led2());
      }
    }
  }
}

void periodic_callbacks__10Hz(uint32_t callback_count) {
  // Add your code here

  if (self_test_done && !(callback_count % 2)) {
    can_bus_handler__rx_all_msgs(&drive_speed, &steer_angle);
    // drive_speed = target_drive_speed; // override for testing
    // steer_angle = 0;
    servo_motor__change_steering_angle(steer_angle);
    motor_logic__update_motor_state(drive_speed);
  }
}

void periodic_callbacks__100Hz(uint32_t callback_count) {
  // Add your code here
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {
  // Add your code here
}
