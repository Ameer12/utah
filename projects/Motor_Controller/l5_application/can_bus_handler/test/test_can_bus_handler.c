#include <stdio.h>
#include <stdlib.h>

#include "unity.h"

#include "Mockcan_bus.h"

#include "project.h"

#include "can_bus_handler.h"

// related to DBC file
static const float min_speed = -10;
static const float max_speed = 10;
static const float speed_scalar = 0.1f;
static const int8_t min_angle = -45;
static const int8_t max_angle = 45;

void setUp(void) { can_bus_handler__reset_motor_mia_counter(); }

void tearDown(void) {}

void test__can_bus_handler__init(void) {
  can__init_ExpectAnyArgsAndReturn(true);
  can__bypass_filter_accept_all_msgs_Expect();
  can__reset_bus_Expect(can1);
  can_bus_handler__init();
}

/**
 * TODO: add more tests to increase robustness.
 */

void test__can_bus_handler__motor_rx__random_valid_speed_and_angle_data(void) {
  const uint8_t message_count = 200;

  // repeatable seed
  srand(32);

  for (size_t count = 0; count < message_count; ++count) {
    can__msg_t can_msg;
    float expected_speed = (((float)rand() / RAND_MAX) * (max_speed - min_speed)) + min_speed;
    int8_t expected_angle = (rand() % (max_angle - min_angle + 1)) + min_angle;

    // encode payload and add message metadata to struct
    can_msg.data.bytes[0] = (uint8_t)(((expected_speed - min_speed) / speed_scalar) + 0.5f);
    can_msg.data.bytes[1] = expected_angle - min_angle;
    can_msg.msg_id = dbc_header_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG.message_id;
    can_msg.frame_fields.data_len = dbc_header_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG.message_dlc;

    can__rx_ExpectAndReturn(can1, &can_msg, 0, true);
    can__rx_IgnoreArg_can_message_ptr();
    can__rx_ReturnThruPtr_can_message_ptr(&can_msg);

    float actual_speed;
    int8_t actual_angle;
    TEST_ASSERT_EQUAL(can_bus_handler__valid_rx_flag, can_bus_handler__motor_rx(&actual_speed, &actual_angle));
    TEST_ASSERT_FLOAT_WITHIN(speed_scalar, expected_speed, actual_speed);
    TEST_ASSERT_EQUAL(expected_angle, actual_angle);
  }
}

void test__can_bus_handler__motor_rx__testing_mia_recovery(void) {
  can__msg_t can_msg;
  float initial_speed = dbc_mia_replacement_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG.DC_MOTOR_DRIVE_SPEED_sig + 2;
  int8_t initial_angle = dbc_mia_replacement_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG.SERVO_STEER_ANGLE_sig + 20;
  float actual_speed = initial_speed;
  int8_t actual_angle = initial_angle;

  // repeatable seed
  srand(24);

  for (uint8_t count = 0; count < dbc_mia_threshold_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG + 2; ++count) {

    // wrong message type
    can_msg.msg_id = 0x7ff;
    can_msg.frame_fields.data_len = 1;
    can__rx_ExpectAndReturn(can1, &can_msg, 0, true);
    can__rx_IgnoreArg_can_message_ptr();
    can__rx_ReturnThruPtr_can_message_ptr(&can_msg);

    if (count == dbc_mia_threshold_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG - 1) {
      TEST_ASSERT_EQUAL(can_bus_handler__valid_rx_flag, can_bus_handler__motor_rx(&actual_speed, &actual_angle));
      TEST_ASSERT_FLOAT_WITHIN(
          speed_scalar, dbc_mia_replacement_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG.DC_MOTOR_DRIVE_SPEED_sig, actual_speed);
      TEST_ASSERT_EQUAL(dbc_mia_replacement_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG.SERVO_STEER_ANGLE_sig, actual_angle);
    } else {
      TEST_ASSERT_EQUAL(can_bus_handler__invalid_rx_flag, can_bus_handler__motor_rx(&actual_speed, &actual_angle));
    }
  }

  // valid data after mia
  float expected_speed = (((float)rand() / RAND_MAX) * (max_speed - min_speed)) + min_speed;
  int8_t expected_angle = (rand() % (max_angle - min_angle + 1)) + min_angle;

  // encode payload and add message metadata to struct
  can_msg.data.bytes[0] = (uint8_t)(((expected_speed - min_speed) / speed_scalar) + 0.5f);
  can_msg.data.bytes[1] = expected_angle - min_angle;
  can_msg.msg_id = dbc_header_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG.message_id;
  can_msg.frame_fields.data_len = dbc_header_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG.message_dlc;
  can__rx_ExpectAndReturn(can1, &can_msg, 0, true);
  can__rx_IgnoreArg_can_message_ptr();
  can__rx_ReturnThruPtr_can_message_ptr(&can_msg);

  TEST_ASSERT_EQUAL(can_bus_handler__valid_rx_flag, can_bus_handler__motor_rx(&actual_speed, &actual_angle));
  TEST_ASSERT_FLOAT_WITHIN(speed_scalar, expected_speed, actual_speed);
  TEST_ASSERT_EQUAL(expected_angle, actual_angle);
}

void test__can_bus_handler__motor_speed_tx__range_of_values(void) {
  const int8_t start_value = -10;
  const int8_t end_value = 10;

  for (int8_t count = start_value; count < end_value; ++count) {
    dbc_RC_CAR_SPEED_READ_MSG_s can_msg_data;
    can__msg_t can_msg;
    float expected_speed = count < min_speed ? min_speed : count > max_speed ? max_speed : count;
    uint64_t expected_encode_value = (uint64_t)(((expected_speed - min_speed) / speed_scalar) + 0.5f) & 0xff;

    can_msg_data.RC_CAR_SPEED_sig = (float)count;
    const dbc_message_header_t header = dbc_encode_RC_CAR_SPEED_READ_MSG(can_msg.data.bytes, &can_msg_data);
    can_msg.msg_id = header.message_id;
    can_msg.frame_fields.data_len = header.message_dlc;

    TEST_ASSERT_EQUAL(dbc_header_RC_CAR_SPEED_READ_MSG.message_id, can_msg.msg_id);
    TEST_ASSERT_EQUAL(dbc_header_RC_CAR_SPEED_READ_MSG.message_dlc, can_msg.frame_fields.data_len);
    TEST_ASSERT_EQUAL(expected_encode_value & 0x00ff, can_msg.data.bytes[0]);
    TEST_ASSERT_EQUAL((expected_encode_value & 0xff00) >> 8, can_msg.data.bytes[1]);

    can__tx_ExpectAndReturn(can1, &can_msg, 0, true);
    can__tx_IgnoreArg_can_message_ptr();

    TEST_ASSERT_TRUE(can_bus_handler__motor_speed_tx(expected_speed));
  }
}

void test__can_bus_handler__motor_heartbeat_tx(void) {

  dbc_MOTOR_HEARTBEAT_MSG_s can_msg_data;
  can__msg_t can_msg;
  const uint8_t pulse = 0xAA;

  can_msg_data.MOTOR_HEARTBEAT_sig = pulse;
  const dbc_message_header_t header = dbc_encode_MOTOR_HEARTBEAT_MSG(can_msg.data.bytes, &can_msg_data);
  can_msg.msg_id = header.message_id;
  can_msg.frame_fields.data_len = header.message_dlc;

  TEST_ASSERT_EQUAL(dbc_header_MOTOR_HEARTBEAT_MSG.message_id, can_msg.msg_id);
  TEST_ASSERT_EQUAL(dbc_header_MOTOR_HEARTBEAT_MSG.message_dlc, can_msg.frame_fields.data_len);
  TEST_ASSERT_EQUAL(pulse, can_msg.data.bytes[0]);

  can__tx_ExpectAndReturn(can1, &can_msg, 0, true);
  can__tx_IgnoreArg_can_message_ptr();

  TEST_ASSERT_TRUE(can_bus_handler__motor_heartbeat_tx());
}

void test__can_bus_handler__rx_all_msgs(void) {
  const size_t msg_count = 6;
  can__msg_t can_msg[msg_count];

  // put heterogeneous messages in expect buffer
  for (size_t count = 0; count < msg_count; ++count) {

    if (!(count % 2)) {
      can_msg[count].msg_id = 0x7ff;
      can_msg[count].frame_fields.data_len = 1;
      can__rx_ExpectAndReturn(can1, &can_msg[count], 0, true);
      can__rx_IgnoreArg_can_message_ptr();
      can__rx_ReturnThruPtr_can_message_ptr(&can_msg[count]);
    } else {
      can_msg[count].data.bytes[0] = (uint8_t)(((count - min_speed) / speed_scalar) + 0.5f);
      can_msg[count].data.bytes[1] = count - min_angle;
      can_msg[count].msg_id = dbc_header_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG.message_id;
      can_msg[count].frame_fields.data_len = dbc_header_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG.message_dlc;
      can__rx_ExpectAndReturn(can1, &can_msg[count], 0, true);
      can__rx_IgnoreArg_can_message_ptr();
      can__rx_ReturnThruPtr_can_message_ptr(&can_msg[count]);
    }
  }

  can__rx_ExpectAndReturn(can1, &can_msg[0], 0, false);
  can__rx_IgnoreArg_can_message_ptr();

  // last valid message will be what is written to pointers
  float expected_speed = msg_count % 2 ? msg_count : msg_count - 1;
  int8_t expected_angle = msg_count % 2 ? msg_count : msg_count - 1;
  float actual_speed;
  int8_t actual_angle;

  TEST_ASSERT_TRUE(can_bus_handler__rx_all_msgs(&actual_speed, &actual_angle));
  TEST_ASSERT_FLOAT_WITHIN(speed_scalar, expected_speed, actual_speed);
  TEST_ASSERT_EQUAL(expected_angle, actual_angle);
}
