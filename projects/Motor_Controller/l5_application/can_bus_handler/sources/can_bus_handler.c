/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Main Module Header */
#include "can_bus_handler.h"

/* Standard Includes */

/* External Includes */
#include "can_bus.h"

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/
static const uint16_t can_bus_handler__baud_rate = 100;
static const uint16_t can_bus_handler__txq_size = 100;
static const uint16_t can_bus_handler__rxq_size = 100;

/**
 * @note Setup for receiving in the 5Hz periodic callback.
 *       Guessing if no message is received in 5 seconds, defaults should be used
 */
const uint32_t dbc_mia_threshold_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG = 25;
static const uint32_t mia_counter_increment = 1;

const dbc_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG_s dbc_mia_replacement_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG = {
    .DC_MOTOR_DRIVE_SPEED_sig = 0, .SERVO_STEER_ANGLE_sig = 0};

static dbc_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG_s can_msg_motor_data;

/***********************************************************************************************************************
 *
 *                                          P U B L I C   F U N C T I O N S
 *
 **********************************************************************************************************************/
void can_bus_handler__init(void) {

  can__init(can1, can_bus_handler__baud_rate, can_bus_handler__rxq_size, can_bus_handler__txq_size, NULL, NULL);
  can__bypass_filter_accept_all_msgs();
  can__reset_bus(can1);
}

void can_bus_handler__reset_motor_mia_counter(void) { can_msg_motor_data.mia_info.mia_counter = 0; }

uint8_t can_bus_handler__motor_rx(float *drive_speed, int8_t *steer_angle) {
  uint8_t retval = can_bus_handler__nothing_to_rx_flag;
  can__msg_t can_msg;

  if (can__rx(can1, &can_msg, 0)) {
    const dbc_message_header_t header = {.message_id = can_msg.msg_id, .message_dlc = can_msg.frame_fields.data_len};
    retval = can_bus_handler__invalid_rx_flag;

    if (dbc_decode_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG(&can_msg_motor_data, header, can_msg.data.bytes) ||
        dbc_service_mia_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG(&can_msg_motor_data, mia_counter_increment)) {
      retval = can_bus_handler__valid_rx_flag;
      *drive_speed = can_msg_motor_data.DC_MOTOR_DRIVE_SPEED_sig;
      *steer_angle = can_msg_motor_data.SERVO_STEER_ANGLE_sig;
    }
  }

  return retval;
}

bool can_bus_handler__rx_all_msgs(float *drive_speed, int8_t *steer_angle) {
  uint8_t rx_retval;
  bool received_valid_data = false;

  do {

    rx_retval = can_bus_handler__motor_rx(drive_speed, steer_angle);
    if (rx_retval == can_bus_handler__valid_rx_flag)
      received_valid_data = true;

  } while (rx_retval);

  return received_valid_data;
}

bool can_bus_handler__motor_speed_tx(float rc_car_speed) {
  bool retval = false;
  dbc_RC_CAR_SPEED_READ_MSG_s can_msg_data;
  can__msg_t can_msg;

  can_msg_data.RC_CAR_SPEED_sig = rc_car_speed;
  const dbc_message_header_t header = dbc_encode_RC_CAR_SPEED_READ_MSG(can_msg.data.bytes, &can_msg_data);
  can_msg.msg_id = header.message_id;
  can_msg.frame_fields.data_len = header.message_dlc;
  if (can__tx(can1, &can_msg, 0)) {
    retval = true;
  }
  return retval;
}

bool can_bus_handler__motor_heartbeat_tx(void) {
  bool retval = false;
  dbc_MOTOR_HEARTBEAT_MSG_s can_msg_data;
  can__msg_t can_msg;
  const uint8_t pulse = 0xAA;

  can_msg_data.MOTOR_HEARTBEAT_sig = pulse;
  const dbc_message_header_t header = dbc_encode_MOTOR_HEARTBEAT_MSG(can_msg.data.bytes, &can_msg_data);
  can_msg.msg_id = header.message_id;
  can_msg.frame_fields.data_len = header.message_dlc;
  if (can__tx(can1, &can_msg, 0)) {
    retval = true;
  }
  return retval;
}

bool can_bus_handler__reset_can_bus_if_bus_goes_off(void) {
  bool retval = false;
  if (can__is_bus_off(can1)) {
    can__reset_bus(can1);
    retval = true;
  }
  return retval;
}
