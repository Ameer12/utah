/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Main Module Header */

/* Standard Includes */
#include <stdio.h>

/* External Includes */
#include "unity.h"

#include "Mockdc_motor.h"
#include "Mockmotor_pid.h"
#include "Mockrpm_sensor.h"
#include "Mockservo_motor.h"

#include "motor_logic.h"

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                          P U B L I C   F U N C T I O N S
 *
 **********************************************************************************************************************/
void test__motor_logic__self_test(void) {

  servo_motor__change_steering_angle_Expect(-45);
  TEST_ASSERT_FALSE(motor_logic__self_test());
  servo_motor__change_steering_angle_Expect(45);
  TEST_ASSERT_FALSE(motor_logic__self_test());
  servo_motor__change_steering_angle_Expect(0);
  TEST_ASSERT_FALSE(motor_logic__self_test());
  dc_motor__change_drive_speed_Expect(-5);
  TEST_ASSERT_FALSE(motor_logic__self_test());
  dc_motor__change_drive_speed_Expect(0);
  TEST_ASSERT_FALSE(motor_logic__self_test());
  dc_motor__change_drive_speed_Expect(5);
  TEST_ASSERT_FALSE(motor_logic__self_test());
  dc_motor__change_drive_speed_Expect(0);
  TEST_ASSERT_TRUE(motor_logic__self_test());
}