/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Main Module Header */
#include "dc_motor.h"

/* Standard Includes */
#include <math.h>

/* External Includes */
#include "gpio.h"
#include "pwm1.h"

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/
static const uint32_t pwm_frequency_in_hz = 100;

/**
 * @note Local variables for making it easy to update the linear conversion used
 *       to convert drive speed to duty cycle percent.
 */
static const float min_duty_cycle = 13.0f; // speed limiting for now
static const float max_duty_cycle = 17.0f;
static const float idle_duty_cycle = 15.0f; // also the offset

static const float min_drive_speed = -10.0f;
static const float max_drive_speed = 10.0f;

static const float degree_to_duty_scalar = (max_duty_cycle - min_duty_cycle) / (max_drive_speed - min_drive_speed);

/***********************************************************************************************************************
 *
 *                                          P U B L I C   F U N C T I O N S
 *
 **********************************************************************************************************************/
void dc_motor__init(void) {

  // using P2.1 PWM1[2]
  gpio__construct_with_function(GPIO__PORT_2, 1, GPIO__FUNCTION_1);
  pwm1__init_single_edge(pwm_frequency_in_hz);
  pwm1__set_duty_cycle(PWM1__2_1, idle_duty_cycle);
}

void dc_motor__change_drive_speed(float drive_speed) {

  // limit to range
  drive_speed =
      drive_speed < min_drive_speed ? min_drive_speed : drive_speed > max_drive_speed ? max_drive_speed : drive_speed;

  float target_duty_cycle_percent = drive_speed * degree_to_duty_scalar + idle_duty_cycle;
  pwm1__set_duty_cycle(PWM1__2_1, target_duty_cycle_percent);
}
