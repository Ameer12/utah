/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Main Module Header */
#include "servo_motor.h"

/* Standard Includes */

/* External Includes */
#include "gpio.h"
#include "pwm1.h"

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/
static const uint32_t pwm_frequency_in_hz = 100;

static const float min_duty_cycle = 10.0f;
static const float max_duty_cycle = 20.0f;
static const float idle_duty_cycle = 15.0f; // also the offset

static const float min_steer_angle = -45.0f;
static const float max_steer_angle = 45.0f;

static const float degree_to_duty_scalar = (max_duty_cycle - min_duty_cycle) / (max_steer_angle - min_steer_angle);

/***********************************************************************************************************************
 *
 *                                          P U B L I C   F U N C T I O N S
 *
 **********************************************************************************************************************/
void servo_motor__init(void) {

  // using P2.4 PWM1[5]
  gpio__construct_with_function(GPIO__PORT_2, 4, GPIO__FUNCTION_1);
  pwm1__init_single_edge(pwm_frequency_in_hz);
  pwm1__set_duty_cycle(PWM1__2_4, idle_duty_cycle);
}

void servo_motor__change_steering_angle(float steering_angle) {

  // limit range
  steering_angle = steering_angle < min_steer_angle
                       ? min_steer_angle
                       : steering_angle > max_steer_angle ? max_steer_angle : steering_angle;

  // linear relationship between steering angle and duty cycle percent
  float target_duty_cycle_percent = steering_angle * degree_to_duty_scalar + idle_duty_cycle;
  pwm1__set_duty_cycle(PWM1__2_4, target_duty_cycle_percent);
}
