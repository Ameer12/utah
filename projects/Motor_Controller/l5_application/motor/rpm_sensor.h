#pragma once
/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Standard Includes */
#include <stdint.h>

/***********************************************************************************************************************
 *
 *                                     F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

/**
 * @brief Initialize the rpm_sensor by setting up pin P2.6
 *        for T2_CAP0.
 */
void rpm_sensor__init(void);

/**
 * @brief Update speed value
 */
void rpm_sensor__update_speed_value(void);

/**
 * @brief  Return the value from the RPM sensor
 * @retval speed_in_kph
 */
float rpm_sensor__get_speed(void);
