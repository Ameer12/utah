#include "board_io.h"
#include "can_bus.h"
#include "gpio.h"

#include <stdio.h>
#include <string.h>

#include "can_bus_initializer.h"
#include "sensor_controller.h"

#include "ultra_sonic_sensors.h"

static dbc_SENSOR_SONARS_ROUTINE_s ultra_sonic_data;

void Sensor_Controller_init(void) {
  initialize_adc_for_ultra_sonic_sensors();
  initialize_pins_for_ultra_sonic_sensor_triggers();
}

bool can__Sensor_Controller__routine_send(dbc_SENSOR_SONARS_ROUTINE_s *sensor_send_data) {
  bool success = dbc_encode_and_send_SENSOR_SONARS_ROUTINE(NULL, sensor_send_data);
  return success;
}

void Sensor_Controller__100hz_handler(uint32_t callback_count) {
  const uint32_t callback_count_modulo_val = 5;
  const uint32_t callback_count_remainder = callback_count % callback_count_modulo_val;

  switch (callback_count_remainder) {
  case 0:
    ultra_sonic_data.SENSOR_SONARS_middle = get_median_distance(FRONT_ULTRA_SONIC);
    ultra_sonic_data.SENSOR_SONARS_rear = get_median_distance(BACK_ULTRA_SONIC);
    ultra_sonic_data.SENSOR_SONARS_left = get_median_distance(LEFT_ULTRA_SONIC);
    ultra_sonic_data.SENSOR_SONARS_right = get_median_distance(RIGHT_ULTRA_SONIC);
    can__Sensor_Controller__routine_send(&ultra_sonic_data);
    break;
  case 1:
    fill_left_and_right_ultra_sonic_distance_buffer();
    sort_ultra_sonic_distances(LEFT_ULTRA_SONIC);
    sort_ultra_sonic_distances(RIGHT_ULTRA_SONIC);
    break;
  case 2:
    break;
  case 3:
    break;
  case 4:
    fill_front_and_back_ultra_sonic_distance_buffer();
    sort_ultra_sonic_distances(FRONT_ULTRA_SONIC);
    sort_ultra_sonic_distances(BACK_ULTRA_SONIC);
    break;
  }
}

dbc_SENSOR_SONARS_ROUTINE_s get_ultra_sonic_data(void) { return ultra_sonic_data; }