#include "ultra_sonic_sensors.h"
#include "adc.h"
#include "board_io.h"
#include "clock.h"
#include "delay.h"
#include "gpio.h"
#include "sys_time.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 15

static const uint8_t sensor_trigger_delay = 20;

static gpio_s front_ultra_sonic_trigger;
static gpio_s back_ultra_sonic_trigger;
static gpio_s left_ultra_sonic_trigger;
static gpio_s right_ultra_sonic_trigger;

static uint16_t front_sensor_distance_inches[BUFFER_SIZE];
static uint16_t back_sensor_distance_inches[BUFFER_SIZE];
static uint16_t left_sensor_distance_inches[BUFFER_SIZE];
static uint16_t right_sensor_distance_inches[BUFFER_SIZE];

static const float SCALING_FACTOR = 8;
static float distance_in_inches;

void initialize_adc_for_ultra_sonic_sensors(void) { adc__initialize(); }

void initialize_pins_for_ultra_sonic_sensor_triggers(void) {
  front_ultra_sonic_trigger = gpio__construct_as_output(GPIO__PORT_0, 5);
  back_ultra_sonic_trigger = gpio__construct_as_output(GPIO__PORT_0, 7);
  right_ultra_sonic_trigger = gpio__construct_as_output(GPIO__PORT_0, 8);
  left_ultra_sonic_trigger = gpio__construct_as_output(GPIO__PORT_0, 9);

  gpio__set_function(front_ultra_sonic_trigger, GPIO__FUNCITON_0_IO_PIN);
  gpio__set_function(back_ultra_sonic_trigger, GPIO__FUNCITON_0_IO_PIN);
  gpio__set_function(right_ultra_sonic_trigger, GPIO__FUNCITON_0_IO_PIN);
  gpio__set_function(left_ultra_sonic_trigger, GPIO__FUNCITON_0_IO_PIN);

  gpio__set(front_ultra_sonic_trigger);
  gpio__set(back_ultra_sonic_trigger);
  gpio__set(right_ultra_sonic_trigger);
  gpio__set(left_ultra_sonic_trigger);
  delay__ms(500);

  gpio__reset(front_ultra_sonic_trigger);
  gpio__reset(back_ultra_sonic_trigger);
  gpio__reset(right_ultra_sonic_trigger);
  gpio__reset(left_ultra_sonic_trigger);
}

void fill_left_and_right_ultra_sonic_distance_buffer(void) {
  uint16_t left_sensor_raw_data, right_sensor_raw_data;
  size_t i;

  memset(right_sensor_distance_inches, 0, 5 * sizeof(uint16_t));
  memset(left_sensor_distance_inches, 0, 5 * sizeof(uint16_t));

  gpio__set(left_ultra_sonic_trigger);
  delay__us(sensor_trigger_delay);
  for (i = 0; i < BUFFER_SIZE; i++) {
    left_sensor_raw_data = adc__get_adc_value(ADC__CHANNEL_2);
    distance_in_inches = ((float)left_sensor_raw_data / SCALING_FACTOR) + 0.5;
    left_sensor_distance_inches[i] = ((uint32_t)distance_in_inches);
  }
  gpio__reset(left_ultra_sonic_trigger);

  gpio__set(right_ultra_sonic_trigger);
  delay__us(sensor_trigger_delay);
  for (i = 0; i < BUFFER_SIZE; i++) {
    right_sensor_raw_data = adc__get_adc_value(ADC__CHANNEL_5);
    distance_in_inches = ((float)right_sensor_raw_data / SCALING_FACTOR) + 0.5;
    right_sensor_distance_inches[i] = ((uint32_t)distance_in_inches);
  }
  gpio__reset(right_ultra_sonic_trigger);
}

void fill_front_and_back_ultra_sonic_distance_buffer(void) {
  uint16_t front_sensor_raw_data, back_sensor_raw_data;
  size_t i;

  memset(front_sensor_distance_inches, 0, 5 * sizeof(uint16_t));
  memset(back_sensor_distance_inches, 0, 5 * sizeof(uint16_t));

  gpio__set(front_ultra_sonic_trigger);
  delay__us(sensor_trigger_delay);
  for (i = 0; i < BUFFER_SIZE; i++) {
    front_sensor_raw_data = adc__get_adc_value(ADC__CHANNEL_4);
    distance_in_inches = ((float)front_sensor_raw_data / SCALING_FACTOR) + 0.5;
    front_sensor_distance_inches[i] = ((uint32_t)distance_in_inches);
  }
  gpio__reset(front_ultra_sonic_trigger);

  gpio__set(back_ultra_sonic_trigger);
  delay__us(sensor_trigger_delay);
  for (i = 0; i < BUFFER_SIZE; i++) {
    back_sensor_raw_data = adc__get_adc_value(ADC__CHANNEL_3);
    distance_in_inches = ((float)back_sensor_raw_data / SCALING_FACTOR) + 0.5;
    back_sensor_distance_inches[i] = ((uint32_t)distance_in_inches);
  }
  gpio__reset(back_ultra_sonic_trigger);
}

static int compare(const void *a, const void *b) { return (*(uint16_t *)a - *(uint16_t *)b); }

void sort_ultra_sonic_distances(ultra_sonic_sensor_position sensor_position) {
  switch (sensor_position) {
  case FRONT_ULTRA_SONIC:
    qsort(front_sensor_distance_inches, BUFFER_SIZE, sizeof(uint16_t), compare);
    break;

  case BACK_ULTRA_SONIC:
    qsort(back_sensor_distance_inches, BUFFER_SIZE, sizeof(uint16_t), compare);
    break;

  case LEFT_ULTRA_SONIC:
    qsort(left_sensor_distance_inches, BUFFER_SIZE, sizeof(uint16_t), compare);
    break;

  case RIGHT_ULTRA_SONIC:
    qsort(right_sensor_distance_inches, BUFFER_SIZE, sizeof(uint16_t), compare);
    break;
  }
}

uint16_t get_median_distance(ultra_sonic_sensor_position sensor_position) {
  size_t median_position = (BUFFER_SIZE) / 2;
  uint16_t distance_val_to_return = 0;

  switch (sensor_position) {
  case FRONT_ULTRA_SONIC:
    distance_val_to_return = front_sensor_distance_inches[median_position];
    break;

  case BACK_ULTRA_SONIC:
    distance_val_to_return = back_sensor_distance_inches[median_position];
    break;

  case LEFT_ULTRA_SONIC:
    distance_val_to_return = left_sensor_distance_inches[median_position];
    break;

  case RIGHT_ULTRA_SONIC:
    distance_val_to_return = right_sensor_distance_inches[median_position];
    break;
  }

  return distance_val_to_return;
}
