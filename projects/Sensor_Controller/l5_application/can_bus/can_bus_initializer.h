#pragma once

#include <stdbool.h>
#include <stdint.h>

bool initiailize_can1_bus(uint32_t baud_rate, uint32_t rxq_size, uint32_t txq_size);
void initiailize_can2_bus(uint32_t baud_rate, uint32_t rxq_size, uint32_t txq_size);