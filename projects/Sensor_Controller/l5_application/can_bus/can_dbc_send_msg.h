#pragma once

#include "board_io.h"
#include "can_bus.h"
#include "gpio.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

bool dbc_send_can_message(void *argument_from_dbc_encode_and_send, uint32_t message_id, const uint8_t bytes[8],
                          uint8_t dlc);
