#include "bridge_controller.h"
#include "FreeRTOS.h"
#include "clock.h"
#include "fake_gps_data.h"
#include "gpio.h"
#include "line_buffer.h"
#include "queue.h"
#include "sensor_controller.h"
#include "uart.h"
#include <stdio.h>

static dbc_GPS_DESTINATION_LOCATION_s gps_destination_location;
static dbc_GPS_DESTINATION_LOCATION_s gps_destination_location_last_sent;

static dbc_SENSOR_SONARS_ROUTINE_s sensor_current_readings;
static dbc_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG_s driver_current_change_speed;
static dbc_COMPASS_HEADING_DISTANCE_s geo_heading_distance;
static dbc_GPS_CURRENT_INFO_s geo_current_info;
static dbc_GPS_CURRENT_DESTINATIONS_DATA_s geo_current_destination;
static dbc_RC_CAR_SPEED_READ_MSG_s motor_speed_read;

const uint32_t dbc_mia_threshold_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG = 600;
const uint32_t dbc_mia_threshold_COMPASS_HEADING_DISTANCE = 1500;
const uint32_t dbc_mia_threshold_GPS_CURRENT_INFO = 1500;
const uint32_t dbc_mia_threshold_GPS_CURRENT_DESTINATIONS_DATA = 1500;
const uint32_t dbc_mia_threshold_RC_CAR_SPEED_READ_MSG = 15000;

const dbc_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG_s dbc_mia_replacement_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG = {254, 254};
const dbc_COMPASS_HEADING_DISTANCE_s dbc_mia_replacement_COMPASS_HEADING_DISTANCE = {400.0, 400.0, 400.0};
const dbc_GPS_CURRENT_INFO_s dbc_mia_replacement_GPS_CURRENT_INFO = {400.0, 400.0};
const dbc_GPS_CURRENT_DESTINATIONS_DATA_s dbc_mia_replacement_GPS_CURRENT_DESTINATIONS_DATA = {400.0, 400.0};
const dbc_RC_CAR_SPEED_READ_MSG_s dbc_mia_replacement_RC_CAR_SPEED_READ_MSG = {254};

// static dbc_BRIDGE_EMERGENCY_STOP_s emergency_stop;
static char line_buffer[256];
static line_buffer_s line;

static uart_e bridge_uart = UART__3;

void Bridge_Controller_init(void) {
  gps_destination_location.DEST_LONGITUDE = 0.0;
  gps_destination_location.DEST_LATITUDE = 0.0;
  gps_destination_location_last_sent.DEST_LONGITUDE = 0.0;
  gps_destination_location_last_sent.DEST_LATITUDE = 0.0;

  /*uart setup*/
  line_buffer__init(&line, line_buffer, sizeof(line_buffer));
  gpio__construct_with_function(GPIO__PORT_4, 28, GPIO__FUNCTION_2);
  gpio__construct_with_function(GPIO__PORT_4, 29, GPIO__FUNCTION_2);
  uart__init(bridge_uart, clock__get_peripheral_clock_hz(), 9600);
  QueueHandle_t rxq_handle = xQueueCreate(50, sizeof(char));
  QueueHandle_t txq_handle = xQueueCreate(50, sizeof(char));
  uart__enable_queues(bridge_uart, rxq_handle, txq_handle);
}

bool can__Bridge_Controller__coordinate_send(void) {
  bool success = false;
  success = dbc_encode_and_send_GPS_DESTINATION_LOCATION(NULL, &gps_destination_location);

  gps_destination_location_last_sent.DEST_LONGITUDE = gps_destination_location.DEST_LONGITUDE;
  gps_destination_location_last_sent.DEST_LATITUDE = gps_destination_location.DEST_LATITUDE;
  return success;
}

bool can_Bridge_Controller__RX_and_send_to_app_Msg(void) {
  can__msg_t can_msg = {0};

  bool can_rx_message_received = false;

  // Receive all messages
  while (can__rx(can1, &can_msg, 0)) {
  }
  const dbc_message_header_t header = {
      .message_id = can_msg.msg_id,
      .message_dlc = can_msg.frame_fields.data_len,
  };
  // If geo data was successfully decoded, hand it over to mobile app
  if (dbc_decode_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG(&driver_current_change_speed, header, can_msg.data.bytes)) {
    Bridge_Controller__Bluetooth_Send_uint32((uint32_t)(driver_current_change_speed.DC_MOTOR_DRIVE_SPEED_sig), 7);
    Bridge_Controller__Bluetooth_Send_uint32((uint32_t)(driver_current_change_speed.SERVO_STEER_ANGLE_sig), 8);
    can_rx_message_received = true;
  }
  if (dbc_decode_COMPASS_HEADING_DISTANCE(&geo_heading_distance, header, can_msg.data.bytes)) {
    can_rx_message_received = true;
  }

  if (dbc_decode_GPS_CURRENT_INFO(&geo_current_info, header, can_msg.data.bytes)) {
    can_rx_message_received = true;
  }

  if (dbc_decode_GPS_CURRENT_DESTINATIONS_DATA(&geo_current_destination, header, can_msg.data.bytes)) {
    can_rx_message_received = true;
  }
  if (dbc_decode_RC_CAR_SPEED_READ_MSG(&motor_speed_read, header, can_msg.data.bytes)) {
    Bridge_Controller__Bluetooth_Send_float((motor_speed_read.RC_CAR_SPEED_sig), 9);
    can_rx_message_received = true;
  }

  return can_rx_message_received;
}

void Bridge_Controller__10hz_handler(void) {

  // TX to can From app
  // Bridge_Controller__transfer_data_from_uart_driver_to_line_buffer();
  // Bridge_Controller__get_line_from_buffer();
  // Bridge_Controller__print_line();

  can_Bridge_Controller__RX_and_send_to_app_Msg();
  Bridge_Controller__get_sensor_data();
  Bridge_Controller__Bluetooth_Send_uint16(sensor_current_readings.SENSOR_SONARS_left, 3);
  Bridge_Controller__Bluetooth_Send_uint16(sensor_current_readings.SENSOR_SONARS_middle, 4);
  Bridge_Controller__Bluetooth_Send_uint16(sensor_current_readings.SENSOR_SONARS_right, 5);
  Bridge_Controller__Bluetooth_Send_uint16(sensor_current_readings.SENSOR_SONARS_rear, 6);
}

void Bridge_Controller__1hz_handler(void) {

  // If new coord to send, send on bus
  bool send_bool = false;

  send_bool = Bridge_Controller__get_coordinates(fake_gps_data());
  if (send_bool)
    can__Bridge_Controller__coordinate_send();
}

void can_Bridge_Controller__manage_mia_msgs_10hz(void) {
  bool message_mia = false;

  const uint32_t mia_increment_value = 100;

  dbc_service_mia_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG(&driver_current_change_speed, mia_increment_value);
  dbc_service_mia_COMPASS_HEADING_DISTANCE(&geo_heading_distance, mia_increment_value);
  dbc_service_mia_GPS_CURRENT_INFO(&geo_current_info, mia_increment_value);
  dbc_service_mia_GPS_CURRENT_DESTINATIONS_DATA(&geo_current_destination, mia_increment_value);
  dbc_service_mia_RC_CAR_SPEED_READ_MSG(&motor_speed_read, mia_increment_value);
}

bool Bridge_Controller__get_coordinates(dbc_GPS_DESTINATION_LOCATION_s *gps_send_data) {
  bool send_bool = false;
  if ((gps_send_data->DEST_LATITUDE != gps_destination_location_last_sent.DEST_LATITUDE) &&
      (gps_send_data->DEST_LONGITUDE != gps_destination_location_last_sent.DEST_LONGITUDE)) {
    gps_destination_location.DEST_LONGITUDE = gps_send_data->DEST_LONGITUDE;
    gps_destination_location.DEST_LATITUDE = gps_send_data->DEST_LATITUDE;
    send_bool = true;
  }
  return send_bool;
}

/* APP TO BRIDGE*/
static void Bridge_Controller__transfer_data_from_uart_driver_to_line_buffer(void) {
  char byte;
  const uint32_t zero_timeout = 0;

  while (uart__get(bridge_uart, &byte, zero_timeout)) {
    line_buffer__add_byte(&line, byte);
  }
}

static void Bridge_Controller__get_line_from_buffer(void) {

  line_buffer__remove_line(&line, line_buffer, sizeof(line_buffer));
}

static void Bridge_Controller__print_line(void) { printf("From UART:%s\n", line_buffer); }

/*BRIDGE TO APP*/
static void Bridge_Controller__Bluetooth_Send_uint16(uint16_t uint_out, uint32_t msg_identifier) {
  char name_send[32];

  uint32_t upcasted_uint_out = (uint32_t)uint_out;
  sprintf(name_send, "%d%d\n", msg_identifier, upcasted_uint_out);

  for (int i = 0; i < sizeof(name_send); i++) {
    uart__put(bridge_uart, name_send[i], 0);

    if (name_send[i] == '\n')
      break;
  }
}
static void Bridge_Controller__Bluetooth_Send_uint32(uint32_t uint_out, uint32_t msg_identifier) {
  char name_send[32];

  sprintf(name_send, "%d%d\n", msg_identifier, uint_out);

  for (int i = 0; i < sizeof(name_send); i++) {
    uart__put(bridge_uart, name_send[i], 0);

    if (name_send[i] == '\n')
      break;
  }
}
static void Bridge_Controller__Bluetooth_Send_float(float float_out, uint32_t msg_identifier) {
  char name_send[32];

  sprintf(name_send, "%d%2.4f\n", msg_identifier, float_out);

  for (int i = 0; i < sizeof(name_send); i++) {
    uart__put(bridge_uart, name_send[i], 0);

    if (name_send[i] == '\n')
      break;
  }
}

static void Bridge_Controller__get_sensor_data(void) {
  sensor_current_readings.SENSOR_SONARS_left = get_ultra_sonic_data().SENSOR_SONARS_left;
  sensor_current_readings.SENSOR_SONARS_middle = get_ultra_sonic_data().SENSOR_SONARS_middle;
  sensor_current_readings.SENSOR_SONARS_right = get_ultra_sonic_data().SENSOR_SONARS_right;
  sensor_current_readings.SENSOR_SONARS_rear = get_ultra_sonic_data().SENSOR_SONARS_rear;
}
